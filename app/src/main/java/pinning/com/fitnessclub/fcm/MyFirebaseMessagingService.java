package pinning.com.fitnessclub.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.Launcher;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;

/**
 * Created by kem on 2018-05-08.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private NotificationManager notificationManager;
    String ADMIN_CHANNEL_ID = "ADMIN_CHANNEL_ID";

    private static final String TAG = "MyFirebaseMsgService";

    public final static String JSON_TAG = "TAG";
    public final static String JSON_ID = "ID";
    //public final static String JSON_TEXT = "TEXT";
    public final static String JSON_TEXT = "DESCRIPTION";
    public final static String JSON_OBJECTTYPE = "OBJECTTYPE";
    public final static String JSON_URL = "URL";
    public final static String JSON_IDQUEUE = "IDQUEUE";
    public final static String JSON_TITLE = "TITLE";

    public final static String TAG_NEWS = "NEWS";
    public final static String TAG_OBJECT = "OBJECT";
    private final static String TAG_FORCEUPDATE = "FORCEUPDATE";
    private final static String TAG_CHANGELINK = "CHANGELINK";
    private final static String TAG_FEEDS = "FEEDS";
    private final static String TAG_RESETACCOUNT = "RESETACCOUNT";
    private final static String TAG_NEWVERSION = "NEWVERSION";
    private final static String TAG_SHAREDIR = "SHAREDIR";
    private final static String TAG_SHAREITEM = "SHAREITEM";
    private final static String TAG_PASSCHANGED = "PASSCHANGED";
    private final static String TAG_REMINDER = "REMINDER";
    private final static String USERPUSH = "USERPUSH";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String message = remoteMessage.getData().get("message");
        String title = remoteMessage.getData().get("title");
        String responseCode = remoteMessage.getData().get("code");
        goTo(responseCode,title,message);


       /* notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Setting up Notification channels for android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
        }
        int notificationId = new Random().nextInt(60000);

        Intent intent = new Intent(getApplicationContext(), Launcher.class);

        PendingIntent pintent = PendingIntent.getService(getApplicationContext(), 155, intent, 0);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_home_icon)  //a resource for your custom small icon
                .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                .setContentText(remoteMessage.getData().get("msg")) //ditto
//                .setAutoCancel(true)
                //dismisses the notification on click
                .setContentIntent(pintent)
                .setSound(defaultSoundUri);

        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId *//* ID of notification *//*, notificationBuilder.build());*/
    }

    public void goTo(String action,String title , String message) {
        switch (action) {
            case "0": // success
                Intent success = new Intent("bcNewMessage");
                success.putExtra(Config.FCM_TITLE,title);
                success.putExtra(Config.FCM_MESSAGE,message);
                sendBroadcast(success);
                break;
            case "1":  // expired
                Intent expired = new Intent("bcNewMessage");
                expired.putExtra(Config.FCM_TITLE,title);
                expired.putExtra(Config.FCM_MESSAGE,message);
                sendBroadcast(expired);
                break;
            case "3":   // Already checked in
                Intent checked = new Intent("bcNewMessage");
                checked.putExtra(Config.FCM_TITLE,title);
                checked.putExtra(Config.FCM_MESSAGE,message);
                sendBroadcast(checked);
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "notifications_admin_channel_name";
        String adminChannelDescription = "notifications_admin_channel_description";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    @Override
    public void onNewToken(String token) {
        Methods.savePre(ApplicationContext.getContext(), token, Config.PREF_KEY_FCM_PUSHID);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }
}
