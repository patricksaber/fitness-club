package pinning.com.fitnessclub.contract;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pinning.com.fitnessclub.model.membership.MembershipData;

import static pinning.com.fitnessclub.provider.MyContentProvider.BASE_CONTENT_URI;
import static pinning.com.fitnessclub.provider.MyContentProvider.CONTENT_AUTHORITY;

/**
 * Created by cobra on 7/19/18.
 */

public class MembershipContract {
    public static final String PATH_LIST_OF_MEMBERSHIP = "listofMembership";


    public static class Entry implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.fitnessclub.membershipList";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.fitnessclub.membershipList";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIST_OF_MEMBERSHIP).build();
        public static final String TABLE_NAME = "membershipList";
        public static final String COL_MEMBERSHIP_ID = "id";
        public static final String COL_MEMBERSHIP_TITILE = "membershipTitle";
        public static final String COL_MEMBERSHIP_PRICE = "membershipPrice";
        public static final String COL_MEMBERSHIP_PRIORITY = "membershipPriority";

    }

    public static void insertUpdate(Context c, List<MembershipData> membershipData) {
        Uri uri = MembershipContract.Entry.CONTENT_URI;
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        // cursor from database
//                PROJECTION = new String[]{"*"};// by def all columns
//        String selectIdConversation = Entry.COL_ID_ITEM_CATEGORY + " = '" + idConversation + "'";
//        String select = ItemContract.Entry.COL_ISREPLIED + " = '" + isReplied + "' AND " + selectIdConversation;
        Cursor mCursor = c.getContentResolver().query(
                uri,  // The content URI of the words table
                null,                       // The columns to return for each row
                null,                   // Either null, or the word the user entered
                null,                    // Either empty, or the string the user entered
                null);

        HashMap<String, MembershipData> entryMap = new HashMap<>(); // list from server
        for (MembershipData e : membershipData) {
//            String getStringId = Integer.toString(e.getCategoryId());
            String getStringId = e.getId();
            entryMap.put(getStringId, e);
        }
        if (mCursor != null) {


            while (mCursor.moveToNext()) {
                MembershipData broadcast = entryMap.get(mCursor.getString(mCursor.getColumnIndex(Entry.COL_MEMBERSHIP_ID)));
                int id = mCursor.getInt(mCursor.getColumnIndex(MembershipContract.Entry._ID));
                if (broadcast != null) {
                    Uri existingUri = MembershipContract.Entry.CONTENT_URI.buildUpon().appendPath(Integer.toString(id)).build();
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(Entry.COL_MEMBERSHIP_ID, broadcast.getId())
                            .withValue(Entry.COL_MEMBERSHIP_TITILE, broadcast.getTitle())
                            .withValue(Entry.COL_MEMBERSHIP_PRICE, broadcast.getPrice())
                            .withValue(Entry.COL_MEMBERSHIP_PRIORITY, broadcast.getPriority())
                            .build());
                    entryMap.remove(broadcast.getId());

                } else {//. delete from database to be synced with server
                    Uri deleteUri = MembershipContract.Entry.CONTENT_URI.buildUpon().appendPath(Integer.toString(id)).build();
                    batch.add(ContentProviderOperation.newDelete(deleteUri).build());
                }
            }
        }
        for (MembershipData broadcast : entryMap.values()) {
            batch.add(ContentProviderOperation.newInsert(MembershipContract.Entry.CONTENT_URI)
                    .withValue(Entry.COL_MEMBERSHIP_ID, broadcast.getId())
                    .withValue(Entry.COL_MEMBERSHIP_TITILE, broadcast.getTitle())
                    .withValue(Entry.COL_MEMBERSHIP_PRICE, broadcast.getPrice())
                    .withValue(Entry.COL_MEMBERSHIP_PRIORITY, broadcast.getPriority())
                    .build());
        }
        try {
            c.getContentResolver().applyBatch(CONTENT_AUTHORITY, batch);
            c.getContentResolver().notifyChange(
                    MembershipContract.Entry.CONTENT_URI, // URI where data was modified
                    null,                           // No local observer
                    false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    public static void insertItems(Context context, List<MembershipData> broadcastArray) {
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
        for (int i = 0; i < broadcastArray.size(); i++) {
//
            batch.add(ContentProviderOperation.newInsert(MembershipContract.Entry.CONTENT_URI)
                    .withValue(Entry.COL_MEMBERSHIP_ID, broadcastArray.get(i).getId())
                    .withValue(Entry.COL_MEMBERSHIP_TITILE, broadcastArray.get(i).getTitle())
                    .withValue(Entry.COL_MEMBERSHIP_PRICE, broadcastArray.get(i).getPrice())
                    .withValue(Entry.COL_MEMBERSHIP_PRIORITY, broadcastArray.get(i).getPriority())
                    .build());


        }
        try {
            context.getContentResolver().applyBatch(CONTENT_AUTHORITY, batch);
            context.getContentResolver().notifyChange(MembershipContract.Entry.CONTENT_URI, null, false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }
/*
    public static void insertGym(Context context, GetGymDetailsResponse broadcast) {
        final ContentResolver contentResolver = context.getContentResolver();
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
        batch.add(ContentProviderOperation.newInsert(Entry.CONTENT_URI)
                .withValue(Entry.COL_GYM_ID, broadcast.getGymId())
                .withValue(Entry.COL_GYM_NAME, broadcast.getGymName())
                .withValue(Entry.COL_GYM_TIME, broadcast.getGymTime())
                .withValue(Entry.COL_GYM_LOCATION, broadcast.getGymLocation())
                .withValue(Entry.COL_GYM_NUMBER, broadcast.getGymNumber())
                .withValue(Entry.COL_GYM_X, broadcast.getGymX())
                .withValue(Entry.COL_GYM_Y, broadcast.getGymY())
                .build());
        try {
            contentResolver.applyBatch(CONTENT_AUTHORITY, batch);
            contentResolver.notifyChange(Entry.CONTENT_URI, null, false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }*/
}

