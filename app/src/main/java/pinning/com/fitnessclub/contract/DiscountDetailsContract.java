package pinning.com.fitnessclub.contract;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pinning.com.fitnessclub.model.Data;
import pinning.com.fitnessclub.model.GetGymDetailsResponse;
import pinning.com.fitnessclub.model.GetUserInfoResponse;
import pinning.com.fitnessclub.model.discount.DiscountData;
import pinning.com.fitnessclub.model.discount.GetDiscountResponse;

import static pinning.com.fitnessclub.provider.MyContentProvider.BASE_CONTENT_URI;
import static pinning.com.fitnessclub.provider.MyContentProvider.CONTENT_AUTHORITY;

/**
 * Created by cobra on 7/19/18.
 */

public class DiscountDetailsContract {
    public static final String PATH_LIST_OF_DISCOUNT = "listofDiscount";


    public static class Entry implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.fitnessclub.discountList";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.fitnessclub.discountList";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIST_OF_DISCOUNT).build();
        public static final String TABLE_NAME = "discountList";
        public static final String COL_DISCOUNT_ID = "id";
        public static final String COL_DISCOUNT_TITILE = "discountTitle";
        public static final String COL_DISCOUNT_DATE = "discountDate";
        public static final String COL_DISCOUNT_PRICE = "discountPrice";
        public static final String COL_DISCOUNT_PRIORITY = "discountPriority";

    }

    public static void insertUpdate(Context c, List<DiscountData> discountData) {
        Uri uri = DiscountDetailsContract.Entry.CONTENT_URI;
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        // cursor from database
//                PROJECTION = new String[]{"*"};// by def all columns
//        String selectIdConversation = Entry.COL_ID_ITEM_CATEGORY + " = '" + idConversation + "'";
//        String select = ItemContract.Entry.COL_ISREPLIED + " = '" + isReplied + "' AND " + selectIdConversation;
        Cursor mCursor = c.getContentResolver().query(
                uri,  // The content URI of the words table
                null,                       // The columns to return for each row
                null,                   // Either null, or the word the user entered
                null,                    // Either empty, or the string the user entered
                null);

        HashMap<String, DiscountData> entryMap = new HashMap<>(); // list from server
        for (DiscountData e : discountData) {
//            String getStringId = Integer.toString(e.getCategoryId());
            String getStringId = e.getId();
            entryMap.put(getStringId, e);
        }
        if (mCursor != null) {


            while (mCursor.moveToNext()) {
                DiscountData broadcast = entryMap.get(mCursor.getString(mCursor.getColumnIndex(Entry.COL_DISCOUNT_ID)));
                int id = mCursor.getInt(mCursor.getColumnIndex(DiscountDetailsContract.Entry._ID));
                if (broadcast != null) {
                    Uri existingUri = DiscountDetailsContract.Entry.CONTENT_URI.buildUpon().appendPath(Integer.toString(id)).build();
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(Entry.COL_DISCOUNT_ID, broadcast.getId())
                            .withValue(Entry.COL_DISCOUNT_TITILE, broadcast.getTitle())
                            .withValue(Entry.COL_DISCOUNT_DATE, broadcast.getDateExpiration())
                            .withValue(Entry.COL_DISCOUNT_PRICE, broadcast.getPrice())
                            .withValue(Entry.COL_DISCOUNT_PRIORITY, broadcast.getPriority())
                            .build());
                    entryMap.remove(broadcast.getId());

                } else {//. delete from database to be synced with server
                    Uri deleteUri = DiscountDetailsContract.Entry.CONTENT_URI.buildUpon().appendPath(Integer.toString(id)).build();
                    batch.add(ContentProviderOperation.newDelete(deleteUri).build());
                }
            }
        }
        for (DiscountData broadcast : entryMap.values()) {
            batch.add(ContentProviderOperation.newInsert(DiscountDetailsContract.Entry.CONTENT_URI)
                    .withValue(Entry.COL_DISCOUNT_ID, broadcast.getId())
                    .withValue(Entry.COL_DISCOUNT_TITILE, broadcast.getTitle())
                    .withValue(Entry.COL_DISCOUNT_DATE, broadcast.getDateExpiration())
                    .withValue(Entry.COL_DISCOUNT_PRICE, broadcast.getPrice())
                    .withValue(Entry.COL_DISCOUNT_PRIORITY, broadcast.getPriority())
                    .build());
        }
        try {
            c.getContentResolver().applyBatch(CONTENT_AUTHORITY, batch);
            c.getContentResolver().notifyChange(
                    DiscountDetailsContract.Entry.CONTENT_URI, // URI where data was modified
                    null,                           // No local observer
                    false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    public static void insertItems(Context context, List<DiscountData> broadcastArray) {
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
        for (int i = 0; i < broadcastArray.size(); i++) {
//
            batch.add(ContentProviderOperation.newInsert(DiscountDetailsContract.Entry.CONTENT_URI)
                    .withValue(Entry.COL_DISCOUNT_ID, broadcastArray.get(i).getId())
                    .withValue(Entry.COL_DISCOUNT_TITILE, broadcastArray.get(i).getTitle())
                    .withValue(Entry.COL_DISCOUNT_DATE, broadcastArray.get(i).getDateExpiration())
                    .withValue(Entry.COL_DISCOUNT_PRICE, broadcastArray.get(i).getPrice())
                    .withValue(Entry.COL_DISCOUNT_PRIORITY, broadcastArray.get(i).getPriority())
                    .build());


        }
        try {
            context.getContentResolver().applyBatch(CONTENT_AUTHORITY, batch);
            context.getContentResolver().notifyChange(DiscountDetailsContract.Entry.CONTENT_URI, null, false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }
/*
    public static void insertGym(Context context, GetGymDetailsResponse broadcast) {
        final ContentResolver contentResolver = context.getContentResolver();
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
        batch.add(ContentProviderOperation.newInsert(Entry.CONTENT_URI)
                .withValue(Entry.COL_GYM_ID, broadcast.getGymId())
                .withValue(Entry.COL_GYM_NAME, broadcast.getGymName())
                .withValue(Entry.COL_GYM_TIME, broadcast.getGymTime())
                .withValue(Entry.COL_GYM_LOCATION, broadcast.getGymLocation())
                .withValue(Entry.COL_GYM_NUMBER, broadcast.getGymNumber())
                .withValue(Entry.COL_GYM_X, broadcast.getGymX())
                .withValue(Entry.COL_GYM_Y, broadcast.getGymY())
                .build());
        try {
            contentResolver.applyBatch(CONTENT_AUTHORITY, batch);
            contentResolver.notifyChange(Entry.CONTENT_URI, null, false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }*/
}

