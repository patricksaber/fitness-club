package pinning.com.fitnessclub.contract;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pinning.com.fitnessclub.model.Data;


import static pinning.com.fitnessclub.provider.MyContentProvider.CONTENT_AUTHORITY;

/**
 * Created by cobra on 7/19/18.
 */

public class GymsDetailsContract {
    public static final String PATH_LIST_OF_GYM = "listofproducts";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static class Entry implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.fitnessclub.gymList";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.fitnessclub.gymList";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIST_OF_GYM).build();
        public static final String TABLE_NAME = "gymList";
        public static final String COL_GYM_ID = "gymid";
        public static final String COL_GYM_NAME = "gymname";
        public static final String COL_GYM_NUMBER = "gymnumber";
        public static final String COL_GYM_LOCATION = "gymlocation";
        public static final String COL_GYM_TIME = "gymtime";
        public static final String COL_GYM_X = "gymx";
        public static final String COL_GYM_Y = "gymy";
        public static final String COL_IMAGE = "image";
    }


    public static void insertItems(Context context, List<Data> broadcastArray) {
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
        for (int i = 0; i < broadcastArray.size(); i++) {
//
            batch.add(ContentProviderOperation.newInsert(GymsDetailsContract.Entry.CONTENT_URI)
                    .withValue(Entry.COL_GYM_ID, broadcastArray.get(i).getGymId())
                    .withValue(Entry.COL_GYM_NAME, broadcastArray.get(i).getGymName())
                    .withValue(Entry.COL_GYM_TIME, broadcastArray.get(i).getGymTime())
                    .withValue(Entry.COL_GYM_LOCATION, broadcastArray.get(i).getGymLocation())
                    .withValue(Entry.COL_GYM_NUMBER, broadcastArray.get(i).getGymNumber())
                    .withValue(Entry.COL_GYM_X, broadcastArray.get(i).getGymX())
                    .withValue(Entry.COL_GYM_Y, broadcastArray.get(i).getGymY())
                    .withValue(Entry.COL_IMAGE, broadcastArray.get(i).getImage())
                    .build());


        }
        try {
            context.getContentResolver().applyBatch(CONTENT_AUTHORITY, batch);
            context.getContentResolver().notifyChange(GymsDetailsContract.Entry.CONTENT_URI, null, false);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }

}
