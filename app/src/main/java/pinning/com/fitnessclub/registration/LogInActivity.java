package pinning.com.fitnessclub.registration;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.parceler.Parcels;
import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.MainActivity;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.connectivity.Factory;
import pinning.com.fitnessclub.model.LoginReq;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.model.StatusCodeResponse;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener, ApiResultReceiver.Receiver{

    public final static String REG_STATUS = "reg_status";
    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();
    Dialog pDialog;
    @BindView(R.id.login_btn)
    AppCompatButton loginBtn;
    @BindView(R.id.password_et)
    AppCompatEditText password;
    @BindView(R.id.username_et)
    AppCompatEditText username;
    @BindView(R.id.signup_tv)
    AppCompatTextView signupBtn;
    @BindView(R.id.keepMeIn)
    AppCompatCheckBox keepMeIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);

        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);
        loginBtn.setOnClickListener(this);
        signupBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btn:
                logIn();
                break;
            case R.id.signup_tv:
                Intent registrationActivity = new Intent(LogInActivity.this,RegistrationActivity.class);
                startActivity(registrationActivity);
                break;
            case R.id.neutral_button:
                pDialog.dismiss();
                break;
        }

    }
    void showLoader() {
        pDialog = new Dialog(LogInActivity.this, R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }
    public void addUserNameToDatabase(String username){
        try {
            DB db = DBFactory.open(this);
            db.put(Config.ACTION_USER_USERNAME,username);
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)){
                    case APIService.ACTION_LOGIN:
                dismissLoader();
                Methods.savePre(LogInActivity.this,Config.LOGIN_COMPLETED, Config.PREF_KEY_REGISTRATION_STATUS);
                Methods.savePre(LogInActivity.this,keepMeIn.isChecked()? Config.REMEMBER_ME: "", Config.PREF_KEY_REMEMBER_ME);
                StatusCodeResponse statusCodeResponse = Parcels.unwrap(resultData.getParcelable("object"));
                if (statusCodeResponse.getStatusCode().getCode()== Factory.statuscode_successful){
                    Intent i = new Intent(LogInActivity.this, MainActivity.class);
                    addUserNameToDatabase(username.getText().toString().trim());
                    startActivity(i);
                    finish();
                }else {
                    pDialog = new Dialog(LogInActivity.this);
                    Methods.showCustomDialog(LogInActivity.this, pDialog,
                            statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),

                            getString(android.R.string.yes), null, null, false, this);
                }
                break;
                }
                break;
                case APIService.STATUS_ERROR:
                    StatusCode statusCode = Parcels.unwrap(resultData.getParcelable("object"));
                    SnackS.snackAlert(LogInActivity.this, statusCode.getMessage());
                    dismissLoader();
                    break;
        }
    }

    public void logIn(){
        LoginReq loginReq = new LoginReq();
        loginReq.setUserName(username.getText().toString().trim());
        loginReq.setPassword(password.getText().toString().trim());
        ConnectionDetector connectionDetector = new ConnectionDetector(LogInActivity.this);
        if (connectionDetector.isConnectingToInternet())
            APIService.apiCall(LogInActivity.this, Parcels.wrap(loginReq), apiResultReceiver, APIService.ACTION_LOGIN);
        else
            SnackS.snackAlert(LogInActivity.this, getString(R.string.no_connection));
    }
}
