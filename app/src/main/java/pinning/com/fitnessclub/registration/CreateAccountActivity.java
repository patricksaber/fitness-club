package pinning.com.fitnessclub.registration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import pinning.com.fitnessclub.R;


public class CreateAccountActivity extends AppCompatActivity {

    public final static String  REG_STATUS = "reg_status";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
    }
}
