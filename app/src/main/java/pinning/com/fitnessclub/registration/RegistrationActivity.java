package pinning.com.fitnessclub.registration;

import android.app.Dialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.connectivity.Factory;
import pinning.com.fitnessclub.model.RegistrationReq;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.model.StatusCodeResponse;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;

public class RegistrationActivity extends AppCompatActivity implements ApiResultReceiver.Receiver,View.OnClickListener{
    @BindView(R.id.firstname_et)
    AppCompatEditText firstname;
    @BindView(R.id.lastname_et)
    AppCompatEditText lastname;
    @BindView(R.id.username_et)
    AppCompatEditText username;
    @BindView(R.id.address_et)
    AppCompatEditText address;
    @BindView(R.id.password_et)
    AppCompatEditText password;
    @BindView(R.id.cpassword_et)
    AppCompatEditText confirmPassword;
    @BindView(R.id.register_button)
    AppCompatButton registerButton;
    @BindView(R.id.signup_facebook)
    AppCompatButton rFacebookButton;
    String a = "", b = "", c = "", d = "", e = "",f = "",g = "";
    boolean didClickBack = false;

    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();
    Dialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);

    }

    @OnClick(R.id.register_button)
    public void RegisterButton(View v) {
        if (firstname.getText().toString().isEmpty()) {
            a = getString(R.string.name_validation) + "\n";
            firstname.setError(a);
        }
        if (!TextUtils.equals(password.getText(), confirmPassword.getText())) {
            b = getString(R.string.password_match) + "\n";
            confirmPassword.setError(b);
        }
        if (password.getText().length() < 8) {
            c = getString(R.string.password_length) + "\n";
            password.setError(c);
        }
        if (password.getText().toString().isEmpty()) {
            d = getString(R.string.lastname_validation) + "\n";
            password.setError(d);
        }
        if (lastname.getText().toString().isEmpty()) {
            e = getString(R.string.name_validation) + "\n";
            lastname.setError(e);
        }
        if (address.getText().length()<5) {
            f = getString(R.string.address_validation) + "\n";
            address.setError(f);
        }
        if (username.getText().length()<5) {
            g = getString(R.string.username_validation) + "\n";
            username.setError(g);
        }
        if (!firstname.getText().toString().isEmpty()&&TextUtils.equals(password.getText(), confirmPassword.getText())
                &&password.getText().length() > 7 &&!password.getText().toString().isEmpty()&&!lastname.getText().toString().isEmpty()
                &&address.getText().length()>5 && username.getText().length()>5){
            RegistrationInit();
        }


    }

    public void RegistrationInit(){

        RegistrationReq registrationReq = new RegistrationReq();
        registrationReq.setFirstname(firstname.getText().toString().trim().toLowerCase());
        registrationReq.setLastname(lastname.getText().toString().trim().toLowerCase());
        registrationReq.setPassword(password.getText().toString().trim());
        registrationReq.setUserName(username.getText().toString().trim());
        registrationReq.setAddress(address.getText().toString().trim());
        registrationReq.setPushId(Methods.getPref(this, Config.PREF_KEY_FCM_PUSHID));
        ConnectionDetector connectionDetector = new ConnectionDetector(RegistrationActivity.this);
        if (connectionDetector.isConnectingToInternet())
            APIService.apiCall(RegistrationActivity.this, Parcels.wrap(registrationReq), apiResultReceiver, APIService.ACTION_REGISTRATION);
        else
            SnackS.snackAlert(RegistrationActivity.this, getString(R.string.no_connection));

    }
    void showLoader() {
        pDialog = new Dialog(RegistrationActivity.this, R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    @Override
    public void onBackPressed() {
        if (didClickBack) {
            super.onBackPressed();
            finish();
        } else {
            didClickBack = true;
            Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    didClickBack = false;
                }
            }, 2000);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)) {
                    case APIService.ACTION_REGISTRATION:
                    dismissLoader();
                    Methods.savePre(RegistrationActivity.this, Config.REGISTRATION_COMPLETED,Config.PREF_KEY_REGISTRATION_STATUS);
                    StatusCodeResponse statusCodeResponse = Parcels.unwrap(resultData.getParcelable("object"));
                    if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_account_created) {
                        pDialog = new Dialog(RegistrationActivity.this);
                        Methods.showCustomDialog(RegistrationActivity.this, pDialog,
                                statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });


                    } else if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_account_existed) {
                        pDialog = new Dialog(RegistrationActivity.this);
                        Methods.showCustomDialog(RegistrationActivity.this, pDialog,
                                statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                getString(android.R.string.yes), null, null, false, this);
                    }
                    break;
                }
                break;
            case APIService.STATUS_ERROR:
                StatusCode statusCode = Parcels.unwrap(resultData.getParcelable("object"));
                SnackS.snackAlert(RegistrationActivity.this, statusCode.getMessage());
                dismissLoader();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.neutral_button:
                pDialog.dismiss();
                break;
        }

    }
}