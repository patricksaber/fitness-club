package pinning.com.fitnessclub.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.google.zxing.integration.android.IntentIntegrator;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pinning.com.fitnessclub.MainActivity;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.connectivity.Factory;
import pinning.com.fitnessclub.model.GetUserInfoReq;
import pinning.com.fitnessclub.model.GetUserInfoResponse;
import pinning.com.fitnessclub.model.RefillRequest;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.model.StatusCodeResponse;
import pinning.com.fitnessclub.registration.RegistrationActivity;
import pinning.com.fitnessclub.utils.CaptureActivityPortrait;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;

public class RefillFragment extends Fragment implements ApiResultReceiver.Receiver,View.OnClickListener {
    @BindView(R.id.scan_barcode)
    AppCompatButton scanBarcode;

    @BindView(R.id.card_number_et)
    AppCompatEditText cardNumber;

    @BindView(R.id.dropDown)
    LinearLayout dropDown;
    @BindView(R.id.instructions)
    LinearLayout instructions;

    @BindView(R.id.dropDownIcon)
    AppCompatImageView dropIcon;
    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();
    DB db;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public RefillFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RefillFragment newInstance(String param1, String param2) {
        RefillFragment fragment = new RefillFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View refill = inflater.inflate(R.layout.fragment_refill, container, false);
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);
        ButterKnife.bind(this,refill);

        return refill;
    }

    @Override
    public void onResume() {
        setupView();
        super.onResume();
    }

    public void setupView(){
        try {
            db = DBFactory.open(getActivity());
            if (db.exists(Config.SCAN_CARD_NUMBER)){
                cardNumber.setText(db.get(Config.SCAN_CARD_NUMBER));
            }
            db.del(Config.SCAN_CARD_NUMBER);
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.scan_barcode)
    public void ScanCode(){
        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan the QRCode");
        integrator.setCameraId(0);
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
        integrator.createScanIntent();


    }
    @OnClick(R.id.refill_account)
    public void OnClickRefill(){
        String username=" ";
        try {
            db = DBFactory.open(getActivity());
             username = db.get(Config.ACTION_USER_USERNAME);
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        RefillRequest refillRequest = new RefillRequest();
        refillRequest.setUserName(username);
        refillRequest.setCardId(cardNumber.getText().toString().trim().toLowerCase());
        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet()) {
            APIService.apiCall(getActivity(), Parcels.wrap(refillRequest), apiResultReceiver, APIService.ACTION_REFILL_ACCOUNT);
        }else {
            SnackS.snackAlert(getActivity(), getString(R.string.no_connection));
        }

    }


    @OnClick(R.id.dropDown)
    public void DropInstruction(View v){
        if (instructions.getVisibility()==v.GONE){
            expand(instructions);
            dropIcon.setImageResource(R.drawable.drowup);
        }else{
            collapse(instructions);
            dropIcon.setImageResource(R.drawable.drop_down);
        }
    }
    private void expand(final View v) {

        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }

        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        v.startAnimation(a);
    }
    Dialog pDialog;
    void showLoader() {
        pDialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.neutral_button:
                pDialog.dismiss();
                break;
        }

    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
       ;
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)) {
                    case APIService.ACTION_REFILL_ACCOUNT:
                        dismissLoader();
                        StatusCodeResponse statusCodeResponse = Parcels.unwrap(resultData.getParcelable("object"));
                        if (statusCodeResponse.getStatusCode().getCode()== Factory.statuscode_account_recharged_successfully){
                            pDialog = new Dialog(getActivity());
                            Methods.showCustomDialog(getActivity(), pDialog,
                                    statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                    getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            MainActivity.isRecharge=true;
                                            pDialog.dismiss();
                                            getActivity().finish();
                                        }
                                    });

                        }else {
                            pDialog = new Dialog(getActivity());
                            Methods.showCustomDialog(getActivity(), pDialog,
                                    statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                    getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            pDialog.dismiss();
                                        }
                                    });
                        }
                        break;

                }
                break;
            case APIService.STATUS_ERROR:
                StatusCode statusCode = Parcels.unwrap(resultData.getParcelable("object"));
                SnackS.snackAlert(getActivity(), statusCode.getMessage());
                dismissLoader();
                break;
        }
    }
}
