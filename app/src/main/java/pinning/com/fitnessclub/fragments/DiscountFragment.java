package pinning.com.fitnessclub.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.parceler.Parcels;
import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.MainActivity;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.adapter.DiscountAdapter;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.connectivity.Factory;
import pinning.com.fitnessclub.contract.DiscountDetailsContract;
import pinning.com.fitnessclub.model.RefillRequest;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.model.StatusCodeResponse;
import pinning.com.fitnessclub.model.discount.GetDiscountResponse;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;


public class DiscountFragment extends Fragment implements DiscountAdapter.OnLocateDiscount, ApiResultReceiver.Receiver, LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView.LayoutManager layoutManager;
    private DiscountAdapter discountAdapter;

    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();

    @BindView(R.id.discount_recyclerView)
    RecyclerView recyclerView;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static String username = "";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public DiscountFragment() {
        // Required empty public constructor
    }


    public static DiscountFragment newInstance(String param1, String param2) {
        DiscountFragment fragment = new DiscountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_discount, container, false);
        ButterKnife.bind(this, v);
        setupView();
        initLoader(0, null, this, getActivity().getSupportLoaderManager());
        GetDiscountInfo();
        return v;
    }

    public void GetDiscountInfo() {
        String nothing = "{}";
        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet())
            APIService.apiCall(getActivity(), Parcels.wrap(nothing), apiResultReceiver, APIService.ACTION_GET_DISCOUNT_DETAILS);
        else
            SnackS.snackAlert(getActivity(), getString(R.string.no_connection));
    }


    private void setupView() {
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        final Cursor cursor = getActivity().getContentResolver().query(DiscountDetailsContract.Entry.CONTENT_URI, null, null, null, DiscountDetailsContract.Entry.COL_DISCOUNT_ID + " ASC");
        if (discountAdapter == null)

            discountAdapter = new DiscountAdapter(getActivity(), cursor, this);
        recyclerView.setAdapter(discountAdapter);

    }

    public static <T> void initLoader(final int loaderId, final Bundle args, final LoaderManager.LoaderCallbacks<T> callbacks, final LoaderManager loaderManager) {
        final Loader<T> loader = loaderManager.getLoader(loaderId);
        if (loader != null && loader.isReset()) {
            loaderManager.restartLoader(loaderId, args, callbacks);
        } else {
            loaderManager.initLoader(loaderId, args, callbacks);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] PROJECTION = null;
        String select = null;
        String sorting = DiscountDetailsContract.Entry.COL_DISCOUNT_ID + " ASC";
        switch (id) {
            case 0:
                return new CursorLoader(getActivity(), DiscountDetailsContract.Entry.CONTENT_URI, PROJECTION, select, null, sorting);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        ((DiscountAdapter) recyclerView.getAdapter()).swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        ((DiscountAdapter) recyclerView.getAdapter()).swapCursor(null);
    }

    Dialog pDialog, dialog;

    void showLoader() {
        pDialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)) {
                    case APIService.ACTION_GET_DISCOUNT_DETAILS:
                        dismissLoader();
                        GetDiscountResponse getDiscountResponse = Parcels.unwrap(resultData.getParcelable("object"));
                        DiscountDetailsContract.insertItems(getActivity(), getDiscountResponse.getDiscountData());
                        break;
                    case APIService.ACTION_BUY_MEMBERSHIP:
                        dismissLoader();
                        StatusCodeResponse statusCodeResponse = Parcels.unwrap(resultData.getParcelable("object"));
                        if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_account_recharged_successfully) {
                            dialog = new Dialog(getActivity());
                            Methods.showCustomDialog(getActivity(), pDialog,
                                    statusCodeResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                    getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            MainActivity.isRecharge = true;
                                            dialog.dismiss();
                                            getActivity().finish();
                                        }
                                    });

                        } else if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_bad_request) {
                            SnackS.snackAlert(getActivity(), statusCodeResponse.getStatusCode().getMessage());

                        } else if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_no_enough_balance) {
                            SnackS.snackAlert(getActivity(), statusCodeResponse.getStatusCode().getMessage());

                        } else if (statusCodeResponse.getStatusCode().getCode() == Factory.statuscode_you_cannot_recharge) {
                            SnackS.snackAlert(getActivity(), statusCodeResponse.getStatusCode().getMessage());

                        }
                }
                break;
            case APIService.STATUS_ERROR:

                StatusCode statusCode = Parcels.unwrap(resultData.getParcelable("object"));
                SnackS.snackAlert(getActivity(), statusCode.getMessage());
                dismissLoader();
                break;
        }
    }

    @Override
    public void onClickDiscount(int position) {
        Cursor cu = discountAdapter.getCursor();
        cu.moveToPosition(position);
        String priority = cu.getString(cu.getColumnIndex(DiscountDetailsContract.Entry.COL_DISCOUNT_PRIORITY));
        String id = cu.getString(cu.getColumnIndex(DiscountDetailsContract.Entry.COL_DISCOUNT_ID));
        String title = cu.getString(cu.getColumnIndex(DiscountDetailsContract.Entry.COL_DISCOUNT_TITILE));

        showVerifyMembershipDialog(title, priority, id);

    }

    public void buyMembership(String cardType, String id) {
        RefillRequest refillRequest = new RefillRequest();

        refillRequest.setUserName(username);
        refillRequest.setCardType(cardType);
        refillRequest.setCardId(id);

        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet()) {
            APIService.apiCall(getActivity(), Parcels.wrap(refillRequest), apiResultReceiver, APIService.ACTION_BUY_MEMBERSHIP);
        } else {
            SnackS.snackAlert(getActivity(), getString(R.string.no_connection));
        }

    }

    void showVerifyMembershipDialog(String title, final String cardType, final String cardId) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.verify_membership) + "\n" + title + " ? ")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes_iam), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
                        if (connectionDetector.isConnectingToInternet())
                            buyMembership(cardType, cardId);
                        else
                            SnackS.snackAlert(getActivity(), getString(R.string.no_connection));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no_edit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}
