package pinning.com.fitnessclub.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.adapter.GymAdapter;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.contract.GymsDetailsContract;
import pinning.com.fitnessclub.model.GetGymDetailsResponse;
import pinning.com.fitnessclub.model.GetUserInfoReq;
import pinning.com.fitnessclub.model.GetUserInfoResponse;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.registration.LogInActivity;
import pinning.com.fitnessclub.utils.SnackS;

public class LocateFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
        ApiResultReceiver.Receiver, GymAdapter.OnLocateGym, GymAdapter.OnLocateClasses {
    private RecyclerView.LayoutManager layoutManager;

    private GymAdapter gymAdapter;
    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();

    @BindView(R.id.gym_recyclerView)
    RecyclerView recyclerView;
    Dialog pDialog;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    public LocateFragment() {
        // Required empty public constructor
    }


    public static LocateFragment newInstance(String param1, String param2) {
        LocateFragment fragment = new LocateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View locateGym = inflater.inflate(R.layout.fragment_locate, container, false);
        ButterKnife.bind(this, locateGym);
        setupView();
        initLoader(0, null, this, getActivity().getSupportLoaderManager());
        GetGymInfo();
        return locateGym;
    }

    private void setupView() {
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        final Cursor cursor = getActivity().getContentResolver().query(GymsDetailsContract.Entry.CONTENT_URI, null, null, null, GymsDetailsContract.Entry.COL_GYM_NAME + " ASC");
        if (gymAdapter == null)

            gymAdapter = new GymAdapter(getActivity(), cursor, this, this);
        recyclerView.setAdapter(gymAdapter);

    }


    public void GetGymInfo() {
        String nothing = "{}";
        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        if (connectionDetector.isConnectingToInternet())
            APIService.apiCall(getActivity(), Parcels.wrap(nothing), apiResultReceiver, APIService.ACTION_GET_GYM_DETAILS);
        else
            SnackS.snackAlert(getActivity(), getString(R.string.no_connection));
    }

    public static <T> void initLoader(final int loaderId, final Bundle args, final LoaderManager.LoaderCallbacks<T> callbacks, final LoaderManager loaderManager) {
        final Loader<T> loader = loaderManager.getLoader(loaderId);
        if (loader != null && loader.isReset()) {
            loaderManager.restartLoader(loaderId, args, callbacks);
        } else {
            loaderManager.initLoader(loaderId, args, callbacks);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] PROJECTION = null;
        String select = null;
        String sorting = GymsDetailsContract.Entry.COL_GYM_ID + " ASC";
        switch (id) {
            case 0:
                return new CursorLoader(getActivity(), GymsDetailsContract.Entry.CONTENT_URI, PROJECTION, select, null, sorting);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        ((GymAdapter) recyclerView.getAdapter()).swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        ((GymAdapter) recyclerView.getAdapter()).swapCursor(null);
    }

    void showLoader() {
        pDialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)) {
                    case APIService.ACTION_GET_GYM_DETAILS:
                        dismissLoader();
                        GetGymDetailsResponse getGymDetailsResponse = Parcels.unwrap(resultData.getParcelable("object"));
                        GymsDetailsContract.insertItems(getActivity(), getGymDetailsResponse.getData());
                        break;
                }
                break;
            case APIService.STATUS_ERROR:
                StatusCode statusCode = Parcels.unwrap(resultData.getParcelable("object"));
                SnackS.snackAlert(getActivity(), statusCode.getMessage());
                dismissLoader();
                break;
        }
    }

    @Override
    public void onClickLocation(int position) {
        Cursor cu = gymAdapter.getCursor();
        cu.moveToPosition(position);
        String lo = cu.getString(cu.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_X));
        String al = cu.getString(cu.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_Y));
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + lo + "," + al);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    public void onClickClasses(int position) {
        Cursor cu = gymAdapter.getCursor();
        cu.moveToPosition(position);
        String image = cu.getString(cu.getColumnIndex(GymsDetailsContract.Entry.COL_IMAGE));
        Intent classes = new Intent(getActivity(), GymSchedule.class);
        classes.putExtra("image",image);
        startActivity(classes);
    }
}