package pinning.com.fitnessclub.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.facebook.drawee.view.SimpleDraweeView;
import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.utils.Methods;

public class GymSchedule extends AppCompatActivity {
    @BindView(R.id.sch_image)
    SimpleDraweeView simpleDraweeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_schedule);
        setStatusBarColor(R.color.yellowDark);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        String image = Methods.getLinkFromPref(ApplicationContext.getContext())+intent.getStringExtra("image");
        simpleDraweeView.setImageURI(Uri.parse(image));

    }

    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(color));
        }
    }
}
