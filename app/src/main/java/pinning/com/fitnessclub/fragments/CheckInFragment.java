package pinning.com.fitnessclub.fragments;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.registration.RegistrationActivity;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;

public class CheckInFragment extends Fragment {

    @BindView(R.id.qrcode_image)
    AppCompatImageView qrCode_image;

    String userCOde;
    DB db;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CheckInFragment() {
        // Required empty public constructor
    }


    public static CheckInFragment newInstance(String param1, String param2) {
        CheckInFragment fragment = new CheckInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Dialog pDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View checkIn = inflater.inflate(R.layout.fragment_check_in, container, false);
        ButterKnife.bind(this, checkIn);


        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // here you can update your db with new messages and update the ui (chat message list)
                String message = intent.getStringExtra(Config.FCM_MESSAGE);
                String title = intent.getStringExtra(Config.FCM_TITLE);

                pDialog = new Dialog(getActivity());
                Methods.customDialog(getActivity(), pDialog, title + "\n" + message
                        , getString(android.R.string.cancel),
                        getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pDialog.dismiss();
                            }
                        });

            }
        }, new IntentFilter("bcNewMessage"));


        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            db = DBFactory.open(getActivity());
            userCOde = db.get(Config.ACTION_USER_ACCOUNT_QRCODE);
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(userCOde, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrCode_image.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return checkIn;
    }


    private final BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String message = intent.getStringExtra(Config.FCM_MESSAGE);
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }
    };
}
