package pinning.com.fitnessclub.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;
import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.utils.Config;



public class AccountFragment extends Fragment {
    @BindView(R.id.username_tv)
    AppCompatTextView username;
    @BindView(R.id.AccountBalance_tv)
    AppCompatTextView balance;
    @BindView(R.id.AccountType_tv)
    AppCompatTextView type;
    @BindView(R.id.AccountValid_tv)
    AppCompatTextView valid;
    DB db;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int section;
    private String mParam2;


    public AccountFragment() {
        // Required empty public constructor
    }


    public static AccountFragment newInstance(int section, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, section);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            section = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        setupView();

        return view;
    }


    private void setupView() {
        try {
            db = DBFactory.open(getActivity());
            username.setText(db.get(Config.ACTION_USER_USERNAME));
            String b = db.get(Config.ACTION_USER_ACCOUNT_BALANCE) + "Points";
            balance.setText(b);
            type.setText(db.get(Config.ACTION_USER_ACCOUNT_TYPE));
            valid.setText(db.get(Config.ACTION_USER_ACCOUNT_VALID));
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }


}
