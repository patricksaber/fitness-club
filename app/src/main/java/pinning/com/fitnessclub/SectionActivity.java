package pinning.com.fitnessclub;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.adapter.FragmentPageAdapter;
import pinning.com.fitnessclub.fragments.AccountFragment;
import pinning.com.fitnessclub.fragments.CheckInFragment;
import pinning.com.fitnessclub.fragments.DiscountFragment;
import pinning.com.fitnessclub.fragments.LocateFragment;
import pinning.com.fitnessclub.fragments.MembershipFragment;
import pinning.com.fitnessclub.fragments.RefillFragment;
import pinning.com.fitnessclub.utils.Config;

public class SectionActivity extends AppCompatActivity {
    public static final String EXTRA_SECTION = "section";

    public static final int SECTION_ACCOUNT = 1;
    public static final int SECTION_DEALS = 2;
    public static final int SECTION_CHECKIN = 3;
    public static final int SECTION_LOCATE = 4;
    private int Section;


    private FragmentPageAdapter adapter;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.section_main_icon)
    AppCompatButton main_icon;
    @BindView(R.id.sections_pager)
    ViewPager viewPager;
    @BindView(R.id.sections_tabs)
    TabLayout tabLayout;
    DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ApplicationContext.getAppTheme());  //to change app theme
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section);
        ButterKnife.bind(this);

        fragmentManager = getSupportFragmentManager();
        Section = getIntent().getIntExtra(EXTRA_SECTION, 0);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_home_icon);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        initLayout(Section);

    }

    @Override
    public void onBackPressed() {
        toolbar.setNavigationIcon(R.drawable.ic_home_icon);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initLayout(final int section) {

        adapter = new FragmentPageAdapter(this, getSupportFragmentManager());

        switch (section) {

            case SECTION_ACCOUNT:
                setStatusBarColor(R.color.blueDark);
                main_icon.setText(getString(R.string.my_account));
                adapter.addFragment(AccountFragment.newInstance(SECTION_ACCOUNT, " "), getString(R.string.account));
                adapter.addFragment(RefillFragment.newInstance("", " "), getString(R.string.refill));

                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                break;

            case SECTION_CHECKIN:
                setStatusBarColor(R.color.greenDark);
                main_icon.setText(getString(R.string.check_in));
                adapter.addFragment(CheckInFragment.newInstance("", " "), getString(R.string.your_id_code));

                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                break;
            case SECTION_DEALS:
                setStatusBarColor(R.color.orangeDark);
                main_icon.setText(getString(R.string.deals_offers));
                adapter.addFragment(DiscountFragment.newInstance("", " "), getString(R.string.offers));
                adapter.addFragment(MembershipFragment.newInstance("", " "), getString(R.string.membership));

                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                break;

            case SECTION_LOCATE:
                setStatusBarColor(R.color.yellowDark);
                main_icon.setText(getString(R.string.locate_bt));
                adapter.addFragment(LocateFragment.newInstance("", " "), getString(R.string.locate_gym));

                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                break;
        }

        setupViewPager(viewPager);
        setupTabLayout(tabLayout);
        //viewPager.setCurrentItem(adapter.getCount()-1);

    }

    public void setupTabLayout(TabLayout tabLayout) {
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(android.R.color.white));
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < adapter.getCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(R.layout.tab_text);
                tab.setText(adapter.getPageTitle(i));
//                ((ViewGroup)tabLayout.getParent()).removeView(tabLayout);
            }
        }
    }

    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(color));
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        //    adapter.addFragment(PlaceholderFragment.newInstance(2), "Test", R.drawable.more);

        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(adapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (Section) {
                    case SECTION_ACCOUNT:
                        switch (position) {

                        }
                        break;
                    case SECTION_CHECKIN:
                        switch (position) {

                        }
                        break;
                    case SECTION_DEALS:
                        switch (position) {

                        }
                        break;
                    case SECTION_LOCATE:
                        switch (position) {
                        }
                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                try {
                    db = DBFactory.open(SectionActivity.this);
                    db.put(Config.SCAN_CARD_NUMBER, result.getContents());
                   /* db.put(Config.ACTION_USER_USERNAME, getUserInfoResponse.getUsername());
                    db.put(Config.ACTION_USER_ACCOUNT_TYPE, getUserInfoResponse.getAccountType());
                    db.put(Config.ACTION_USER_ACCOUNT_VALID, getUserInfoResponse.getValid());
                    db.put(Config.ACTION_USER_ACCOUNT_BALANCE, getUserInfoResponse.getBalance());
                    db.put(Config.ACTION_USER_ACCOUNT_QRCODE,getUserInfoResponse.getQrcode());*/
                    db.close();
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}
