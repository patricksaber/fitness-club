
package pinning.com.fitnessclub.connectivity;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;

import com.google.gson.Gson;

import org.parceler.Parcels;

import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.model.GetGymDetailsResponse;
import pinning.com.fitnessclub.model.GetUserInfoResponse;
import pinning.com.fitnessclub.model.StatusCode;
import pinning.com.fitnessclub.model.StatusCodeResponse;
import pinning.com.fitnessclub.model.discount.GetDiscountResponse;
import pinning.com.fitnessclub.model.membership.GetMembershipResponse;
import pinning.com.fitnessclub.utils.Encryptor;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.MyLogs;
import retrofit2.Call;
import retrofit2.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */

public class APIService extends IntentService {

    public static final int STATUS_STARTED = 0;
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_ERROR = 2;


    public static final int CONNECTION_ERROR = -1;
    public static final int OPERATION_SUCCESSFUL = 0;


    public static final String ACTION = "action.ACTION";

    public static final String ACTION_LOGIN = "action.login";
    public static final String ACTION_REGISTRATION = "action.registration";
    public static final String ACTION_GET_USER_INFO = "action.get.user.info";
    public static final String ACTION_GET_GYM_DETAILS = "action.get.gym.details";
    public static final String ACTION_REFILL_ACCOUNT = "action.refill.account";
    public static final String ACTION_GET_DISCOUNT_DETAILS = "action.discount.details";
    public static final String ACTION_GET_MEMBERSHIP_DETAILS = "action.membership.details";
    public static final String ACTION_BUY_MEMBERSHIP = "action.buy.membership";
//    public static final String ACTION_INITIALIZATION = "action.initialization";
//    public static final String ACTION_REFRESH_FAVORITES = "action.refresh.favorites";
//    public static final String ACTION_VALIDATE_PINCODE = "action.validate.pincode";
//    public static final String ACTION_RESEND_PIN = "action.resend.pincode";
//    public static final String ACTION_UPDATE_SETTINGS = "action.update.settings";
//    public static final String ACTION_UNSUBSCRIBE = "action.unsubscribe";
//    public static final String ACTION_BOOK_TAG = "action.book.tag";
//    public static final String ACTION_SEARCH_TAG = "action.search.tag";
//    public static final String ACTION_NOTIFY_TAG = "acion.notify.tag";
//    public static final String ACTION_RESET_PASSWORD = "action.reset.password";
//    public static final String ACTION_LOGIN = "action.login";
//    public static final String ACTION_CREATE_LOGIN = "action.create.login";
//    public static final String ACTION_CHECK_IS_VPN = "action.check.is.vpn";
//    public static final String ACTION_CHECK_VPN_EMAIL = "action.check.vpn.email";
//    public static final String ACTION_UPDATE_REDIRECTION = "action.update.redirection.response";

    public APIService() {
        super("NTAPIService");
    }


    private MyApiEndpointInterface apiCall;
    private Call<String> call;


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null)
            call.cancel();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyLogs.debug("APIService started");
        if (apiCall == null) {
            apiCall = Factory.create();
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }


    /**
     * Starts this service to perform action Search with the given userID. If
     * the service is already performing a task this action will be queued.
     */

    public static Intent apiCall(Context context, Parcelable object, ResultReceiver mReceiver, String action) {
        Intent intent = new Intent(context, APIService.class);
        intent.setAction(action);
        intent.putExtra("receiver", mReceiver);
        intent.putExtra("object", object);
        context.startService(intent);
        return intent;
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            Bundle bundle = new Bundle();
            ResultReceiver receiver = intent.getParcelableExtra("receiver");

            try {
                /* Update UI: Download Service is Running */
                String action = intent.getAction();
                bundle.putString(ACTION, action);
                receiver.send(STATUS_STARTED, bundle);
                switch (action) {
//                    case ACTION_REGISTRATION_INIT:
//                        SetAttend setAttend = Parcels.unwrap(intent.getExtras().getParcelable("object"));
//                        bundle.putParcelable(DATA_SENT, setAttend.getIsAttend());
//                        call = apiCall.RegistrationInit(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//
                    case ACTION_LOGIN:
                        call = apiCall.Login(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
                        break;

                    case ACTION_REGISTRATION:
                        call = apiCall.RegistrationInit(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
                        break;
                    case ACTION_REFILL_ACCOUNT:
                        call = apiCall.Recharge(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
                        break;
                    case ACTION_GET_USER_INFO:
                        call = apiCall.GetUserInfo(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
                        break;
                    case ACTION_BUY_MEMBERSHIP:
                        call = apiCall.BuyMembership(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
                        break;

                    case ACTION_GET_GYM_DETAILS:
                        call = apiCall.GetGymDetails();
                        break;
                    case ACTION_GET_DISCOUNT_DETAILS:
                        call = apiCall.GetDiscountDetails();
                        break;
                    case ACTION_GET_MEMBERSHIP_DETAILS:
                        call = apiCall.GetMembershipDetails();
                        break;
//                    case ACTION_RESEND_PIN:
//                        call = apiCall.ResendPincode();
//                        break;
//                    case ACTION_INITIALIZATION:
//                        call = apiCall.Initialization(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_UPDATE_SETTINGS:
//                        call = apiCall.UpdateSettings(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_UNSUBSCRIBE:
//                        call = apiCall.Unsubscribe(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_UPDATE_REDIRECTION:
//                        call = apiCall.UpdateRedirection(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_BOOK_TAG:
//                        call = apiCall.BookTag(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_SEARCH_TAG:
//                        call = apiCall.SearchTag(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_NOTIFY_TAG:
//                        call = apiCall.NotifyTag(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_RESET_PASSWORD:
//                        call = apiCall.ResetPassword(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_LOGIN:
//                        call = apiCall.Login(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_CREATE_LOGIN:
//                        call = apiCall.CreateLogin(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_CHECK_IS_VPN:
//                        call = apiCall.CheckIsVPN(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
//                    case ACTION_CHECK_VPN_EMAIL:
//                        call = apiCall.CheckVPNEmail(postData(Parcels.unwrap(intent.getExtras().getParcelable("object"))));
//                        break;
                }

                if (call != null)
                    handleHit(receiver, bundle, call);

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                if (receiver != null) {
                    StatusCode statusCode = new StatusCode();
                    statusCode.setCode(CONNECTION_ERROR);
                    statusCode.setMessage(getString(R.string.no_connection));
                    bundle.putParcelable("object", Parcels.wrap(statusCode));
                    receiver.send(STATUS_ERROR, bundle);
                }
            }
        }
//        }
    }




    /*private String postData(Object object) {
        String stringJson = null;
        final String encDeckey = Methods.doYourWork();
        final Gson gson = new Gson();
        String post = gson.toJson(object).toString();
        String xxx = Methods.removeDoubleQuotes(post);
        try {
            stringJson = Encryptor.encrypt(encDeckey, xxx);
        } catch (Exception x) {
            stringJson = null;
        }

        return stringJson;
    }*/

    private void handleHit(ResultReceiver receiver, Bundle bundle, Call<String> call) throws Exception {

        Response<String> response = call.execute();
        if (response.code() == 200) {
            String resp = response.body();
            resp = Methods.removeDoubleQuotes(resp);
//            String encDeckey = Methods.doYourWork(ApplicationContext.getContext());
//            String decResp = Encryptor.decrypt(encDeckey, resp);
            bundle.putString(Intent.EXTRA_TEXT, resp);
            parseObjects(receiver, bundle);

        } else {
            StatusCode statusCode = new StatusCode();
            statusCode.setCode(CONNECTION_ERROR);
            statusCode.setMessage(response.message());
            bundle.putParcelable("object", Parcels.wrap(statusCode));

            receiver.send(STATUS_ERROR, bundle);
        }

    }

    private String postData(Object object) {
        String stringJson = null;
//        final String encDeckey = Methods.doYourWork(ApplicationContext.getContext());//Methods.getPref(this, Config.PREF_KEY_ENC_DEC);
        final Gson gson = new Gson();
        String post = gson.toJson(object).toString();
        String xxx = Methods.removeDoubleQuotes(post);
//        try {
//            stringJson = Encryptor.encrypt(encDeckey, xxx);
//        } catch (Exception x) {
//            stringJson = null;
//        }

        return xxx;
    }

    private void parseObjects(ResultReceiver receiver, Bundle bundle) {
        Gson gson = new Gson();
        switch (bundle.getString(APIService.ACTION)) {
//
            case APIService.ACTION_LOGIN:
                StatusCodeResponse statusCodeResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
                bundle.putParcelable("object", Parcels.wrap(statusCodeResponse));
                receiver.send(STATUS_SUCCESS, bundle);

                break;
            case APIService.ACTION_REGISTRATION:
                StatusCodeResponse RegistrationStatusCodeResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
                bundle.putParcelable("object", Parcels.wrap(RegistrationStatusCodeResponse));
                receiver.send(STATUS_SUCCESS, bundle);
                break;

            case APIService.ACTION_GET_USER_INFO:
                GetUserInfoResponse getUserInfoResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), GetUserInfoResponse.class);
                bundle.putParcelable("object", Parcels.wrap(getUserInfoResponse));
                receiver.send(STATUS_SUCCESS, bundle);
                break;

            case APIService.ACTION_GET_GYM_DETAILS:
                GetGymDetailsResponse getGymDetailsResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), GetGymDetailsResponse.class);
                if (isSuccess(getGymDetailsResponse.getStatusCode(), receiver, bundle)) {
                    bundle.putParcelable("object", Parcels.wrap(getGymDetailsResponse));
                    receiver.send(STATUS_SUCCESS, bundle);
                }
                break;

            case APIService.ACTION_GET_DISCOUNT_DETAILS:
                GetDiscountResponse getDiscountResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), GetDiscountResponse.class);
                if (isSuccess(getDiscountResponse.getStatusCode(), receiver, bundle)) {
                    bundle.putParcelable("object", Parcels.wrap(getDiscountResponse));
                    receiver.send(STATUS_SUCCESS, bundle);
                }
                break;

            case APIService.ACTION_GET_MEMBERSHIP_DETAILS:
                GetMembershipResponse getMembershipResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), GetMembershipResponse.class);
                if (isSuccess(getMembershipResponse.getStatusCode(), receiver, bundle)) {
                    bundle.putParcelable("object", Parcels.wrap(getMembershipResponse));
                    receiver.send(STATUS_SUCCESS, bundle);
                }
                break;

            case APIService.ACTION_REFILL_ACCOUNT:
                StatusCodeResponse r = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);

                bundle.putParcelable("object", Parcels.wrap(r));
                receiver.send(STATUS_SUCCESS, bundle);

                break;

            case APIService.ACTION_BUY_MEMBERSHIP:
                StatusCodeResponse StatusCodeResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);

                    bundle.putParcelable("object", Parcels.wrap(StatusCodeResponse));
                    receiver.send(STATUS_SUCCESS, bundle);

                break;

//
//            case APIService.ACTION_UPDATE_REDIRECTION:
//                UpdateRedirectionResponse updateRedirectionResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), UpdateRedirectionResponse.class);
//                if (isSuccess(updateRedirectionResponse.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(updateRedirectionResponse));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_VALIDATE_PINCODE:
//                StatusCodeResponse validatePincodeRsp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(validatePincodeRsp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(validatePincodeRsp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_RESEND_PIN:
//                ResendPincodeResponse resendPincodeRsp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), ResendPincodeResponse.class);
//                if (isSuccess(resendPincodeRsp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(resendPincodeRsp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_INITIALIZATION:
//                InitializationResponse initResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), InitializationResponse.class);
//                if (isSuccess(initResp.getStatusCode(), receiver, bundle)) {
//
//                    Methods.savePrefObject(initResp.getAppInfo(), Config.PREF_KEY_APP_INFO, ApplicationContext.getContext());
//                    Methods.savePrefObject(initResp.getBookedTags(), Config.PREF_KEY_BOOKED_TAGS, ApplicationContext.getContext());
//                    Methods.savePrefObject(initResp.getRecommendedTags(), Config.PREF_KEY_RECOMMENDED_TAGS,  ApplicationContext.getContext());
//                    Methods.savePrefObject(initResp.getTopSearchedTags(), Config.PREF_KEY_TOP_SEARCHED_TAGS,  ApplicationContext.getContext());
//                    Methods.savePrefObject(initResp.getDocumentLink(), Config.PREF_KEY_DOCUMENTS_LINKS,  ApplicationContext.getContext());
//                    Methods.savePrefObject(initResp.getSettings(), Config.PREF_KEY_APP_SETTINGS,  ApplicationContext.getContext());
//
//                    bundle.putParcelable("object", Parcels.wrap(initResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_UPDATE_SETTINGS:
//                StatusCodeResponse updateSettingsResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(updateSettingsResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(updateSettingsResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_UNSUBSCRIBE:
//                StatusCodeResponse unsubResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(unsubResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(unsubResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_BOOK_TAG:
//                BookTagResponse bookTagResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), BookTagResponse.class);
//                if (isSuccess(bookTagResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(bookTagResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_NOTIFY_TAG:
//                StatusCodeResponse notifyTagResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(notifyTagResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(notifyTagResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_SEARCH_TAG:
//                SearchTagResponse searchTagResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), SearchTagResponse.class);
//                if (isSuccess(searchTagResponse.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(searchTagResponse));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_RESET_PASSWORD:
//                StatusCodeResponse resetPasswordResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(resetPasswordResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(resetPasswordResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//
//
//            case APIService.ACTION_LOGIN:
//                StatusCodeResponse loginResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(loginResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(loginResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//            case APIService.ACTION_CREATE_LOGIN:
//                StatusCodeResponse createLoginResp = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), StatusCodeResponse.class);
//                if (isSuccess(createLoginResp.getStatusCode(), receiver, bundle)) {
//                    bundle.putParcelable("object", Parcels.wrap(createLoginResp));
//                    receiver.send(STATUS_SUCCESS, bundle);
//                }
//                break;
//
//            case APIService.ACTION_CHECK_IS_VPN:
//                CheckIsVPNResponse checkIsVPNResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), CheckIsVPNResponse.class);
//
//                    bundle.putParcelable("object", Parcels.wrap(checkIsVPNResponse));
//                    receiver.send(STATUS_SUCCESS, bundle);
//
//                break;
//            case APIService.ACTION_CHECK_VPN_EMAIL:
//                CheckVPNEmailResponse checkVPNEmailResponse = gson.fromJson(bundle.getString(Intent.EXTRA_TEXT), CheckVPNEmailResponse.class);
//
//                    bundle.putParcelable("object", Parcels.wrap(checkVPNEmailResponse));
//                    receiver.send(STATUS_SUCCESS, bundle);
//
//                break;


        }
    }

    private boolean isSuccess(StatusCode statusCode, ResultReceiver receiver, Bundle bundle) {

        switch (statusCode.getCode()) {
            case OPERATION_SUCCESSFUL:
                return true;
        }

        bundle.putParcelable("object", Parcels.wrap(statusCode));
        receiver.send(STATUS_ERROR, bundle);
        return false;
    }
}

