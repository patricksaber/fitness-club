package pinning.com.fitnessclub.connectivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Encryptor;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;
import retrofit2.Retrofit;

/**
 * Created by abpa on 3/20/2017.
 */
public class Factory {

    public static final String BASE_URL = Methods.getLinkFromPref(ApplicationContext.getContext());
    public static final String ENDPOINT = BASE_URL + "/api/";

    public static MyApiEndpointInterface create() {


        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(1, TimeUnit.MINUTES);
        builder.connectTimeout(1, TimeUnit.MINUTES);
        //builder.addInterceptor(new HeaderInterceptor());

        builder.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = null;
                try {
                    request = chain.request().newBuilder()
                            /* .addHeader(Config.header_imei, Encryptor.encrypt(Config.EncryktionKey, Methods.getIMEI(ApplicationContext.getContext())))
                             .addHeader(Config.header_idHandset, Config.HANDSET_ANDROID)
                             .addHeader(Config.header_idLanguage, Methods.getPref(ApplicationContext.getContext(), Config.PREF_KEY_ID_LANGUAGE))
                             .addHeader(Config.header_appversion, Methods.getAppVersion(ApplicationContext.getContext()))
                             .addHeader(Config.header_osVersion, Methods.getOSversion())
                             .addHeader(Config.header_pushId, Encryptor.encrypt(Config.EncryktionKey, Methods.getPref(ApplicationContext.getContext(), Config.PREF_KEY_GCM_PUSHID)))
                             .addHeader(Config.header_mcc, Methods.Get_MCC(ApplicationContext.getContext()))
                             .addHeader(Config.header_mnc, Methods.Get_MNC(ApplicationContext.getContext()))
                             .addHeader(Config.header_deviceModel, Methods.getDeviceName())*/
                            .addHeader(Config.header_imei, Encryptor.encrypt(Config.EncryptionKey, Methods.getIMEI(ApplicationContext.getContext())))
                            .addHeader(Config.header_idHandset, Config.HANDSET_ANDROID)
                            .addHeader(Config.header_idAppLanguage, Methods.getLanguageNumber())
                            .addHeader(Config.header_appVersion, Encryptor.encrypt(Config.EncryptionKey, Methods.getAppVersion(ApplicationContext.getContext())))
                            .addHeader(Config.header_osVersion, Encryptor.encrypt(Config.EncryptionKey, Methods.getOSversion()))
                            .addHeader(Config.header_pushId, Methods.getPref(ApplicationContext.getContext(), Config.PREF_KEY_FCM_PUSHID))
                            .addHeader(Config.header_idUser, /*Encryptor.encrypt(Config.EncryptionKey, */Methods.getPref(ApplicationContext.getContext(), Config.PREF_KEY_USER_ID).equals("") ? "0" : Methods.getPref(ApplicationContext.getContext(), Config.PREF_KEY_USER_ID))
                            // .addHeader("Content-Type","application/json")
                            .build();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return chain.proceed(request);
            }
        });

        OkHttpClient client = builder.build();


        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(client)
                .addConverterFactory(new ToStringConverterFactory())
                .build();

        return retrofit.create(MyApiEndpointInterface.class);
    }


    public static void showDialog(final Activity mContext, String messageToDisplay, final boolean exitContext) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage(messageToDisplay);
        alertDialogBuilder
                .setNeutralButton(mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (exitContext)
                            mContext.finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }


    public static final int statuscode_successful = 0;
    public static final int statuscode_account_created = 2;
    public static final int statuscode_account_existed = 3;
    public static final int statuscode_account_recharged_successfully = 7;
    public static final int statuscode_bad_request = 8;
    public static final int statuscode_no_enough_balance = 9;
    public static final int statuscode_you_cannot_recharge = 10;

    public static final int statuscode_error_occured = 1;
    public static final int statuscode_encryption_error = 3;
    public static final int statuscode_wrong_password = 101;
    public static final int statuscode_wrong_pin = 104;
    public static final int statuscode_email_attempts_exceeded = 110;
    public static final int statucode_pin_attempts_exceeded = 111;
    public static final int statuscode_resend_pin_attempts_exceeded = 112;
    public static final int statuscode_access_denied = 204;
    public static final int statuscode_invalid_parameters = 206;
    public static final int statuscode_email_verification_failed = 216;
    public static final int statuscode_pincode_sent_to_user = 217;
    public static final int statuscode_proceed_to_vpn_email = 218;


//    public static boolean isSuccess(Activity mContext, StatusCode statusCode) {
//
//        switch (statusCode.getCode()) {
//
//            case statuscode_encryption_error:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statuscode_access_denied:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statuscode_error_occured:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statuscode_invalid_parameters:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statuscode_email_attempts_exceeded:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statuscode_resend_pin_attempts_exceeded:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//            case statuscode_wrong_password:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//            case statuscode_wrong_pin:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            case statucode_pin_attempts_exceeded:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//
//            case statuscode_email_verification_failed:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//
//            case statuscode_proceed_to_vpn_email:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return false;
//
//            default:
//                SnackS.snackAlert(mContext, statusCode.getMessage());
//                return true;
//        }
//
//    }


}
