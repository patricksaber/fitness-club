package pinning.com.fitnessclub.connectivity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by abpa on 5/02/2018.
 */
public interface MyApiEndpointInterface {

//

    //
//    @POST("Registration/ValidatePinCode")
//    Call<String> ValidatePincode(@Body String validatePincodeReq);
    @POST("user/customers")
    Call<String> RegistrationInit(@Body String registrationInitReq);

    @POST("user/Login")
    Call<String> Login(@Body String loginReq);

    @POST("user/GetUserInfo")
    Call<String> GetUserInfo(@Body String GetUserInfoReq);
    @POST("user/BuyMembership")
    Call<String> BuyMembership(@Body String BuyMembershipReq);

    @GET("user/GetGymDetails")
    Call<String> GetGymDetails();

    @GET("user/GetDiscountInfo")
    Call<String> GetDiscountDetails();

    @GET("user/GetMembershipInfo")
    Call<String> GetMembershipDetails();

    @GET("privacy")
    Call<String> PrivacyPolicy();

    @POST("user/Recharge")
    Call<String> Recharge(@Body String refillRequest);
//    @POST("UserData/Initialization")
//    Call<String> Initialization(@Body String initializationReq);
//
//    @POST("UserData/UpdateSettings")
//    Call<String> UpdateSettings(@Body String updateSettingsReq);
//
//    @POST("UserData/Unsubscribe")
//    Call<String> Unsubscribe(@Body String unsubscribeRequest);
//
//    @POST("UserData/BookTag")
//    Call<String> BookTag(@Body String bookTagRequest);
//
//    @POST("UserData/SearchTag")
//    Call<String> SearchTag(@Body String searchTagRequest);
//
//    @POST("UserData/NotifyTag")
//    Call<String> NotifyTag(@Body String notifyTagReq);
//
//    @POST("UserData/ResetPassword")
//    Call<String> ResetPassword(@Body String resetPasswordReq);
//
//    @POST("UserData/Login")
//    Call<String> Login(@Body String loginReq);
//
//    @POST("UserData/CreateLogin")
//    Call<String> CreateLogin(@Body String createLoginReq);
//    @POST("UserData/CheckIsVPN")
//    Call<String> CheckIsVPN(@Body String createLoginReq);
//    @POST("UserData/CheckVPNEmail")
//    Call<String> CheckVPNEmail(@Body String createLoginReq);
//    @POST("UserData/updateredirection")
//    Call<String> UpdateRedirection(@Body String updateRedirection);


}
