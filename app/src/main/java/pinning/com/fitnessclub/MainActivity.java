package pinning.com.fitnessclub;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import pinning.com.fitnessclub.connectivity.APIService;
import pinning.com.fitnessclub.connectivity.ApiResultReceiver;
import pinning.com.fitnessclub.connectivity.ConnectionDetector;
import pinning.com.fitnessclub.fragments.AccountFragment;
import pinning.com.fitnessclub.model.GetUserInfoReq;
import pinning.com.fitnessclub.model.GetUserInfoResponse;
import pinning.com.fitnessclub.registration.LogInActivity;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.SnackS;

import static pinning.com.fitnessclub.fragments.DiscountFragment.username;
import static pinning.com.fitnessclub.fragments.MembershipFragment.usernameMembership;

public class MainActivity extends AppCompatActivity implements ApiResultReceiver.Receiver, View.OnClickListener {

    @BindView(R.id.myAccount_bt)
    AppCompatButton myAccount;
    @BindView(R.id.deals_bt)
    AppCompatButton deals;
    @BindView(R.id.checkIn_bt)
    AppCompatButton checkIn;
    @BindView(R.id.locate_bt)
    AppCompatButton locate;

    @BindView(R.id.swich)
    AppCompatTextView new_feeds;

    @BindView(R.id.fab_logout)
    FloatingActionButton logout;
    @BindView(R.id.fab_settings)
    FloatingActionButton settings;


    ApiResultReceiver apiResultReceiver;
    Handler uiHandler = new Handler();
    Dialog pDialog;
    Toolbar toolbar;

    DB db;
    private int S_ACCOUNT = 1;
    private int S_DEALS = 2;
    private int S_CHECKIN = 3;
    private int S_LOCATE = 4;
    String tag;

    boolean didClickBack = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        apiResultReceiver = new ApiResultReceiver(uiHandler);
        apiResultReceiver.setReceiver(this);
        try {
            getUserInfo();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        myAccount.setOnClickListener(this);
        deals.setOnClickListener(this);
        checkIn.setOnClickListener(this);
        locate.setOnClickListener(this);
        settings.setOnClickListener(this);
        new_feeds.setOnClickListener(this);
        logout.setOnClickListener(this);


    }

    public static boolean isRecharge = false;

    @Override
    protected void onResume() {
        super.onResume();
        if (isRecharge) {
            try {
                getUserInfo();
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            isRecharge = false;
        }

    }

    public void getUserInfo() throws SnappydbException {
        String userName = " ";
        db = DBFactory.open(MainActivity.this);
        if (db.exists(Config.ACTION_USER_USERNAME)) {
            userName = db.get(Config.ACTION_USER_USERNAME);
            username = userName;
            usernameMembership = userName;
        }
        db.close();
        GetUserInfoReq getUserInfoReq = new GetUserInfoReq();
        getUserInfoReq.setUsername(userName);
        ConnectionDetector connectionDetector = new ConnectionDetector(MainActivity.this);
        if (connectionDetector.isConnectingToInternet())
            APIService.apiCall(MainActivity.this, Parcels.wrap(getUserInfoReq), apiResultReceiver, APIService.ACTION_GET_USER_INFO);
        else
            SnackS.snackAlert(MainActivity.this, getString(R.string.no_connection));
    }


    @Override
    public void onBackPressed() {
        if (didClickBack) {
            super.onBackPressed();
            finish();
        } else {
            didClickBack = true;
            Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    didClickBack = false;
                }
            }, 2000);
        }
    }

    void showLoader() {
        pDialog = new Dialog(MainActivity.this, R.style.ThemeDialogCustom);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setContentView(R.layout.progressbar_view);
        pDialog.show();
    }

    void dismissLoader() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case APIService.STATUS_STARTED:
                showLoader();
                break;
            case APIService.STATUS_SUCCESS:
                switch (resultData.getString(APIService.ACTION)) {
                    case APIService.ACTION_GET_USER_INFO:
                        dismissLoader();
                        GetUserInfoResponse getUserInfoResponse = Parcels.unwrap(resultData.getParcelable("object"));
                        if (getUserInfoResponse.getData() != null) {
                            try {
                                db = DBFactory.open(MainActivity.this);
                                db.put(Config.ACTION_USER_USERNAME, getUserInfoResponse.getData().getUserName());
                                db.put(Config.ACTION_USER_ACCOUNT_TYPE, getUserInfoResponse.getData().getAccountType());
                                db.put(Config.ACTION_USER_ACCOUNT_VALID, getUserInfoResponse.getData().getValid());
                                db.put(Config.ACTION_USER_ACCOUNT_BALANCE, getUserInfoResponse.getData().getBalance());
                                db.put(Config.ACTION_USER_ACCOUNT_QRCODE, getUserInfoResponse.getData().getQrcode());
                                db.close();
                            } catch (SnappydbException e) {
                                e.printStackTrace();
                            }
                        } else {
                            pDialog = new Dialog(this);
                            Methods.showCustomDialog(this, pDialog,
                                    getUserInfoResponse.getStatusCode().getMessage(), getString(android.R.string.cancel),
                                    getString(android.R.string.yes), null, null, false, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Methods.savePre(MainActivity.this, "", Config.PREF_KEY_REGISTRATION_STATUS);
                                            pDialog.dismiss();
                                            finish();
                                        }
                                    });
                        }


                        break;

                }
                break;
            case APIService.STATUS_ERROR:
                dismissLoader();
                break;
        }
    }

    private void startMyAccountActivity(int tag) {
        ApplicationContext.setAppTheme(R.style.AppThemeBlue);
        startActivity(new Intent(this, SectionActivity.class)
                .putExtra(SectionActivity.EXTRA_SECTION, tag).putExtra("TAG", tag));
    }

    private void startDealsActivity(int tag) {
        ApplicationContext.setAppTheme(R.style.AppThemeGreen);
        startActivity(new Intent(this, SectionActivity.class)
                .putExtra(SectionActivity.EXTRA_SECTION, tag));
    }


    private void startCheckInActivity(int tag) {
        ApplicationContext.setAppTheme(R.style.AppThemeOrange);
        startActivity(new Intent(this, SectionActivity.class)
                .putExtra(SectionActivity.EXTRA_SECTION, tag));
    }

    private void startLocatetActivity(int tag) {
        ApplicationContext.setAppTheme(R.style.AppThemeYellow);
        startActivity(new Intent(this, SectionActivity.class)
                .putExtra(SectionActivity.EXTRA_SECTION, tag));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myAccount_bt:
                startMyAccountActivity(S_ACCOUNT);
                break;
            case R.id.checkIn_bt:
                startDealsActivity(S_CHECKIN);
                break;
            case R.id.deals_bt:
                startCheckInActivity(S_DEALS);
                break;
            case R.id.locate_bt:
                startLocatetActivity(S_LOCATE);
                break;
            case R.id.swich:
                break;
            case R.id.fab_logout:

                Methods.savePre(MainActivity.this, "", Config.PREF_KEY_REGISTRATION_STATUS);
                Intent login = new Intent(this, LogInActivity.class);
                startActivity(login);
                finish();
                break;
            case R.id.fab_settings:
                break;
        }
    }
}
