package pinning.com.fitnessclub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import pinning.com.fitnessclub.registration.LogInActivity;
import pinning.com.fitnessclub.utils.Config;
import pinning.com.fitnessclub.utils.LocaleHelper;
import pinning.com.fitnessclub.utils.Methods;
import pinning.com.fitnessclub.utils.MyLogs;


/**
 * Created by abpa on 12/27/2016.
 */

public class Launcher extends Activity {

    public static final String EXTRA_FROM_WHERE = "EXTRA_FROM_WHERE";
    public static final String FROM_NOTIFICATION = "1";
    public static final String EXTRA_TAG_ID = "EXTRA_TAG_ID";

    String fromWhere;
    int idTag;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }

        MyLogs.info("PUSH_ID===\n"+Methods.getPref(this, Config.PREF_KEY_FCM_PUSHID));
        switch (Methods.getPref(this, Config.PREF_KEY_REGISTRATION_STATUS)) {

            case Config.REMEMBER_ME:
                if (Methods.getPref(Launcher.this, Config.PREF_KEY_REMEMBER_ME).equals(Config.REMEMBER_ME)) {
                    startActivity(new Intent(Launcher.this, MainActivity.class));
                }
                break;

            case Config.LOGIN_COMPLETED:

                if (Methods.getPref(Launcher.this, Config.PREF_KEY_REMEMBER_ME).equals(Config.REMEMBER_ME)) {
                    startActivity(new Intent(Launcher.this, MainActivity.class));
                } else {
                    Intent intent = new Intent(Launcher.this, LogInActivity.class);
                    startActivity(intent);
//                    if (getIntent().getBooleanExtra(CreatePasswordActivity.EXTRA_FROM_CREATE_PASSWORD, false)) {
//                        startActivity(new Intent(Launcher.this, MainActivity.class));
//                    } else {
//                        Intent splashIntent2 = new Intent(this, LoginActivity.class);
//
//                        startActivity(splashIntent2);
//                    }
                }
                finish();
                break;


            default:
                startActivity(new Intent(this, LogInActivity.class));
                finish();
                break;

        }


    }

}
