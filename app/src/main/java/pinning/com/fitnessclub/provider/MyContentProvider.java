package pinning.com.fitnessclub.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import pinning.com.fitnessclub.ApplicationContext;
import pinning.com.fitnessclub.contract.DiscountDetailsContract;
import pinning.com.fitnessclub.contract.GymsDetailsContract;
import pinning.com.fitnessclub.contract.MembershipContract;

public class MyContentProvider extends ContentProvider {

    MyDatabase mDatabaseHelper;

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new MyDatabase(getContext());
        return true;
    }

    public static final String CONTENT_AUTHORITY = ApplicationContext.getContext().getPackageName();


    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    /**
     * Content authority for this provider.
     */
    public static final String AUTHORITY = CONTENT_AUTHORITY;

    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using sUriMatcher.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through sUriMatcher, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.
    /**
     * URI ID for route: /entries
     */
    /**
     * URI ID for route: /entries
     */


    public static final int ROUTE_GYM = 1;
    public static final int ROUTE_GYM_ID = 2;
    public static final int ROUTE_DISCOUNT = 3;
    public static final int ROUTE_DISCOUNT_ID = 4;
    public static final int ROUTE_MEMBERSHIP = 5;
    public static final int ROUTE_MEMBERSHIP_ID = 6;


    /**
     * UriMatcher, used to decode incoming URIs.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, GymsDetailsContract.PATH_LIST_OF_GYM, ROUTE_GYM);
        sUriMatcher.addURI(AUTHORITY, GymsDetailsContract.PATH_LIST_OF_GYM + "/*", ROUTE_GYM_ID);
        sUriMatcher.addURI(AUTHORITY, DiscountDetailsContract.PATH_LIST_OF_DISCOUNT, ROUTE_DISCOUNT);
        sUriMatcher.addURI(AUTHORITY, DiscountDetailsContract.PATH_LIST_OF_DISCOUNT + "/*", ROUTE_DISCOUNT_ID);
        sUriMatcher.addURI(AUTHORITY, MembershipContract.PATH_LIST_OF_MEMBERSHIP, ROUTE_MEMBERSHIP);
        sUriMatcher.addURI(AUTHORITY, MembershipContract.PATH_LIST_OF_MEMBERSHIP + "/*", ROUTE_MEMBERSHIP_ID);

    }

    /**
     * Determine the mime type for entries returned by a given URI.
     */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_GYM:
                return GymsDetailsContract.Entry.CONTENT_TYPE;
            case ROUTE_GYM_ID:
                return GymsDetailsContract.Entry.CONTENT_ITEM_TYPE;
            case ROUTE_DISCOUNT:
                return DiscountDetailsContract.Entry.CONTENT_TYPE;
            case ROUTE_DISCOUNT_ID:
                return DiscountDetailsContract.Entry.CONTENT_ITEM_TYPE;
            case ROUTE_MEMBERSHIP:
                return MembershipContract.Entry.CONTENT_TYPE;
            case ROUTE_MEMBERSHIP_ID:
                return MembershipContract.Entry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        int uriMatch = sUriMatcher.match(uri);
        Context ctx = getContext();
        Cursor cc;
        String idcall = "";
        switch (uriMatch) {
            case ROUTE_GYM_ID:
                idcall = uri.getLastPathSegment();
                builder.where(GymsDetailsContract.Entry._ID + "=?", idcall);
            case ROUTE_GYM:
                builder.table(GymsDetailsContract.Entry.TABLE_NAME).where(selection, selectionArgs);
                cc = builder.query(db, projection, sortOrder);
                assert ctx != null;
                cc.setNotificationUri(ctx.getContentResolver(), uri);
                return cc;
            case ROUTE_DISCOUNT_ID:
                idcall = uri.getLastPathSegment();
                builder.where(DiscountDetailsContract.Entry._ID + "=?", idcall);
            case ROUTE_DISCOUNT:
                builder.table(DiscountDetailsContract.Entry.TABLE_NAME).where(selection, selectionArgs);
                cc = builder.query(db, projection, sortOrder);
                assert ctx != null;
                cc.setNotificationUri(ctx.getContentResolver(), uri);
                return cc;
            case ROUTE_MEMBERSHIP_ID:
                idcall = uri.getLastPathSegment();
                builder.where(MembershipContract.Entry._ID + "=?", idcall);
            case ROUTE_MEMBERSHIP:
                builder.table(MembershipContract.Entry.TABLE_NAME).where(selection, selectionArgs);
                cc = builder.query(db, projection, sortOrder);
                assert ctx != null;
                cc.setNotificationUri(ctx.getContentResolver(), uri);
                return cc;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        assert db != null;
        final int match = sUriMatcher.match(uri);
        Uri result;
        long idc;
        switch (match) {
            case ROUTE_GYM:
                idc = db.insertOrThrow(GymsDetailsContract.Entry.TABLE_NAME, null, values);
                result = Uri.parse(GymsDetailsContract.Entry.CONTENT_URI + "/" + idc);
                break;
            case ROUTE_GYM_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);

            case ROUTE_DISCOUNT:
                idc = db.insertOrThrow(DiscountDetailsContract.Entry.TABLE_NAME, null, values);
                result = Uri.parse(DiscountDetailsContract.Entry.CONTENT_URI + "/" + idc);
                break;
            case ROUTE_DISCOUNT_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);

            case ROUTE_MEMBERSHIP:
                idc = db.insertOrThrow(MembershipContract.Entry.TABLE_NAME, null, values);
                result = Uri.parse(MembershipContract.Entry.CONTENT_URI + "/" + idc);
                break;
            case ROUTE_MEMBERSHIP_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
    }

    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        String idc = "";
        switch (match) {
            case ROUTE_GYM:
                count = builder.table(GymsDetailsContract.Entry.TABLE_NAME).where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_GYM_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(GymsDetailsContract.Entry.TABLE_NAME).where(GymsDetailsContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_DISCOUNT:
                count = builder.table(DiscountDetailsContract.Entry.TABLE_NAME).where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_DISCOUNT_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(DiscountDetailsContract.Entry.TABLE_NAME).where(DiscountDetailsContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_MEMBERSHIP:
                count = builder.table(MembershipContract.Entry.TABLE_NAME).where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_MEMBERSHIP_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(MembershipContract.Entry.TABLE_NAME).where(MembershipContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs).delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an etry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        String idc = "";
        switch (match) {
            case ROUTE_GYM:
                count = builder.table(GymsDetailsContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_GYM_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(GymsDetailsContract.Entry.TABLE_NAME)
                        .where(GymsDetailsContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_DISCOUNT:
                count = builder.table(DiscountDetailsContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_DISCOUNT_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(DiscountDetailsContract.Entry.TABLE_NAME)
                        .where(DiscountDetailsContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_MEMBERSHIP:
                count = builder.table(MembershipContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_MEMBERSHIP_ID:
                idc = uri.getLastPathSegment();
                count = builder.table(MembershipContract.Entry.TABLE_NAME)
                        .where(MembershipContract.Entry._ID + "=?", idc)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * SQLite backend for @{link MyContentProvider}.
     * <p>
     * Provides access to an disk-backed, SQLite datastore which is utilized by MyContentProvider. This
     * database should never be accessed by other parts of the application directly.
     */
    static public class MyDatabase extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "fitnessClub.db";

        private static final String TYPE_TEXT = " TEXT";
        private static final String TYPE_INTEGER = " INTEGER";
        private static final String COMMA_SEP = ",";

        /**
         * SQL statement to create "recordings" table.
         */

        private static final String SQL_CREATE_GYM_LIST_CATEGORY =
                "CREATE TABLE " + GymsDetailsContract.Entry.TABLE_NAME + " (" +
                        GymsDetailsContract.Entry._ID + " INTEGER PRIMARY KEY," +
                        GymsDetailsContract.Entry.COL_GYM_ID + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_NAME + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_LOCATION + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_NUMBER + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_TIME + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_X + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_GYM_Y + TYPE_TEXT + COMMA_SEP +
                        GymsDetailsContract.Entry.COL_IMAGE + TYPE_TEXT + COMMA_SEP +
                        " UNIQUE(" + GymsDetailsContract.Entry.COL_GYM_ID + ") ON CONFLICT REPLACE" + ");";

        private static final String SQL_CREATE_DISCOUNT_LIST_CATEGORY =
                "CREATE TABLE " + DiscountDetailsContract.Entry.TABLE_NAME + " (" +
                        DiscountDetailsContract.Entry._ID + " INTEGER PRIMARY KEY," +
                        DiscountDetailsContract.Entry.COL_DISCOUNT_ID + TYPE_TEXT + COMMA_SEP +
                        DiscountDetailsContract.Entry.COL_DISCOUNT_TITILE + TYPE_TEXT + COMMA_SEP +
                        DiscountDetailsContract.Entry.COL_DISCOUNT_DATE + TYPE_TEXT + COMMA_SEP +
                        DiscountDetailsContract.Entry.COL_DISCOUNT_PRICE + TYPE_TEXT + COMMA_SEP +
                        DiscountDetailsContract.Entry.COL_DISCOUNT_PRIORITY + TYPE_TEXT + COMMA_SEP +
                        " UNIQUE(" + DiscountDetailsContract.Entry.COL_DISCOUNT_ID + ") ON CONFLICT REPLACE" + ");";

        private static final String SQL_CREATE_MEMBERSHIP_LIST_CATEGORY =
                "CREATE TABLE " + MembershipContract.Entry.TABLE_NAME + " (" +
                        MembershipContract.Entry._ID + " INTEGER PRIMARY KEY," +
                        MembershipContract.Entry.COL_MEMBERSHIP_ID + TYPE_TEXT + COMMA_SEP +
                        MembershipContract.Entry.COL_MEMBERSHIP_TITILE + TYPE_TEXT + COMMA_SEP +
                        MembershipContract.Entry.COL_MEMBERSHIP_PRICE + TYPE_TEXT + COMMA_SEP +
                        MembershipContract.Entry.COL_MEMBERSHIP_PRIORITY + TYPE_TEXT + COMMA_SEP +
                        " UNIQUE(" + MembershipContract.Entry.COL_MEMBERSHIP_ID + ") ON CONFLICT REPLACE" + ");";

        /**
         * SQL statement to drop "{TABLE_NAME}" table.
         */


        private static final String SQL_DELETE_GYM_LIST_CATEGORY = "DROP TABLE IF EXISTS " + GymsDetailsContract.Entry.TABLE_NAME;
        private static final String SQL_DELETE_DISCOUNT_LIST_CATEGORY = "DROP TABLE IF EXISTS " + DiscountDetailsContract.Entry.TABLE_NAME;
        private static final String SQL_DELETE_MEMBERSHIP_LIST_CATEGORY = "DROP TABLE IF EXISTS " + MembershipContract.Entry.TABLE_NAME;

        public MyDatabase(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_GYM_LIST_CATEGORY);
            db.execSQL(SQL_CREATE_DISCOUNT_LIST_CATEGORY);
            db.execSQL(SQL_CREATE_MEMBERSHIP_LIST_CATEGORY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_GYM_LIST_CATEGORY);
            db.execSQL(SQL_DELETE_DISCOUNT_LIST_CATEGORY);
            db.execSQL(SQL_DELETE_MEMBERSHIP_LIST_CATEGORY);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_GYM_LIST_CATEGORY);
            db.execSQL(SQL_DELETE_DISCOUNT_LIST_CATEGORY);
            db.execSQL(SQL_DELETE_MEMBERSHIP_LIST_CATEGORY);
            onCreate(db);
        }
    }
}