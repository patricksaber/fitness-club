package pinning.com.fitnessclub.utils;


import pinning.com.fitnessclub.ApplicationContext;

/**
 * Created by abpa on 3/28/2018.
 */

public class Config {

    public static final String TAG = "NAMETAG";
    public static final String PREFS_NAME = "GC_PREFS";
    public static final String HANDSET_ANDROID = "1";
    public static final String EncryptionKey = "3ZLRCUbT8bc9d53W";
    public static final String PREF_KEY_COUNTRY_NAME = "PREF_KEY_COUNTRY_NAME";
    public static final String PREF_KEY_COUNTRY_ISO = "PREF_KEY_COUNTRY_ISO";
    public static final String PREF_KEY_COUNTRY_KEY_CODE = "PREF_KEY_COUNTRY_KEY_CODE";
    public static final String PREF_KEY_APP_LINK = "PREF_KEY_APP_LINK";
    public static final String PREF_KEY_IMEI = "PREF_KEY_IMEI";
    public static final String PREF_KEY_REGISTRATION_STATUS = "PREF_KEY_REGISTRATION_STATUS";
    public static final String PREF_KEY_USER_ID = "PREF_KEY_USER_ID";
    public static final String PREF_KEY_FCM_PUSHID = "PREF_KEY_FCM_PUSHID";
    public static final String PREF_KEY_IMSI_REG = "PREF_KEY_IMSI_REG";
    public static final String PREF_KEY_REMEMBER_ME = "PREF_KEY_REMEMBER_ME";


    public static final String FCM_TITLE = "FCM_TITLE";
    public static final String FCM_MESSAGE = "FCM_MESSAGE";


    public static final String ACTION = ApplicationContext.getContext().getPackageName();
    public static final String ACTION_FORCEUPDATE = ACTION + "ACTION_FORCEUPDATE";

    public static final String ACTION_USER_USERNAME = "username";
    public static final String SCAN_CARD_NUMBER = "card_number";
    public static final String ACTION_USER_ACCOUNT_TYPE = "account_type";
    public static final String ACTION_USER_ACCOUNT_VALID = "account_valid";
    public static final String ACTION_USER_ACCOUNT_BALANCE = "account_balance";
    public static final String ACTION_USER_ACCOUNT_QRCODE = "account_qrcode";

    public static final String REGISTRATION_ALREADY_HAVE_PINCODE = "REGISTRATION_ALREADY_HAVE_PINCODE";
    public static final String REGISTRATION_PAYMENT_SUCCESSFUL = "REGISTRATION_PAYMENT_SUCCESSFUL";
    public static final String REGISTRATION_CREATE_LOGIN = "REGISTRATION_CREATE_LOGIN";
    public static final String REGISTRATION_COMPLETED = "REGISTRATION_COMPLETED";
    public static final String LOGIN_COMPLETED = "LOGIN_COMPLETED";
    public static final String TERMS_AND_CONDITIONS_LINK_EN = "";
    public static final String TERMS_AND_CONDITIONS_LINK_AR = "";
    public static final String PRIVACY_POLICY_LINK_EN = "";
    public static final String PRIVACY_POLICY_LINK_AR = "";

    public static final String REMEMBER_ME = "REMEMBER_ME";

    public static final String NOTIFICATION_FORCEUPDATE_URL = "NOTIFICATION_FORCEUPDATE_URL";

    public static final String header_imei = "imei";
    public static final String header_idUser = "idUser";
    public static final String header_idHandset = "idHandset";
    public static final String header_idAppLanguage = "idAppLanguage";
    public static final String header_appVersion = "appVersion";
    public static final String header_osVersion = "osVersion";
    public static final String header_pushId = "pushId";

    public static final int TAG_TYPE_BOOKED = 1;
    public static final int TAG_TYPE_RECOMMENDED = 2;
    public static final int TAG_TYPE_TOP_SEARCHED = 3;
    public static final int TAG_TYPE_HEADER = 0;
    public static final int TAG_TYPE_SEARCH = 4;

    public static final String SUBSCRIPTION_TYPE_CORPORATE = "SUBSCRIPTION_TYPE_CORPORATE";
    public static final String SUBSCRIPTION_TYPE_MEMBER = "SUBSCRIPTION_TYPE_MEMBER";

    public static final String BOOK_TYPE_NORMAL = "1";
    public static final String BOOK_TYPE_PBX = "2";


}
