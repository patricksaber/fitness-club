package pinning.com.fitnessclub.utils;


import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor {

	public static String encrypt(String seed, String cleartext) throws Exception {
		byte[] rawKey = getRawKey(seed.getBytes());
		byte[] result = encrypt(rawKey, cleartext.getBytes(("UTF-8")));
		return toBase64(result);
	}

	public static String decrypt(String seed, String encrypted) throws Exception {
		String dataReturned = "";
		try {
			byte[] rawKey = getRawKey(seed.getBytes(("UTF-8")));
			byte[] enc = fromBase64(encrypted);
			byte[] result = decrypt(rawKey, enc);
			dataReturned = new String(result);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				String seedStatic= Config.EncryptionKey;
				byte[] rawKey = getRawKey(seedStatic.getBytes(("UTF-8")));
				byte[] enc = fromBase64(encrypted);
				byte[] result = decrypt(rawKey, enc);
				dataReturned = new String(result);
			} catch (Exception e1) {
				e1.printStackTrace();
				JSONObject statuscode = new JSONObject();
				JSONObject code = new JSONObject();
				code.put("code", 9999);
				code.put("messageToDisplay", "EnxDec Error");
				statuscode.put("StatusCode", code);
				try {
					dataReturned = statuscode.toString();
				} catch (Exception e2) {
					dataReturned = "0";
				}
			}
		}
		return dataReturned;
	}

	private static byte[] getRawKey(byte[] seed) throws Exception {
		SecretKey skey = new SecretKeySpec(seed, "AES");

		byte[] raw = skey.getEncoded();

		return raw;
	}

	private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec ivParameterSpec = new IvParameterSpec(raw);

		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
		byte[] encrypted = cipher.doFinal(clear);
		return encrypted;
	}

	private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec ivParameterSpec = new IvParameterSpec(raw);

		cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
		byte[] decrypted = cipher.doFinal(encrypted);
		return decrypted;
	}

	public static String toBase64(byte[] in){
		byte [] budd = in ;
		return b64.encode1(in);
	}

	public static byte[] fromBase64(String str) throws Exception {
		return b64.decode1(str);
	}
}