package pinning.com.fitnessclub.utils;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import java.util.Locale;
import pinning.com.fitnessclub.R;

/**
 * Created by abpa on 3/28/2018.
 */

public class Methods {


    public static int getScreenWidth(Context a) {
        WindowManager window = (WindowManager) a.getSystemService(Context.WINDOW_SERVICE);
        return window.getDefaultDisplay().getWidth();
    }

    public static int getScreenHeight(Context a) {
        WindowManager window = (WindowManager) a.getSystemService(Context.WINDOW_SERVICE);
        return window.getDefaultDisplay().getHeight();
    }

    static public String getPref(Context c, String STOTREDkey) {
        String mobileNum = "";
        try {
            SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
            mobileNum = settings.getString(STOTREDkey, "");
            if (mobileNum.length() > 0)
                mobileNum = Encryptor.decrypt(getEncryptionSP(), mobileNum);
        } catch (Exception ex) {
        }
        return mobileNum;
    }

    static public int getPrefInt(Context c, String STOTREDkey) {
        int num = 1;
        try {
            SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
            String mobileNum = settings.getString(STOTREDkey, "");
            num = Integer.parseInt(mobileNum);
        } catch (Exception ex) {
        }

        return num;
    }

    static public Object getPrefObject(String Key, Context context, Class<?> myClass) {
        Object value = null;
        try {
            SharedPreferences settings = context.getSharedPreferences(Config.PREFS_NAME, 0);
            Gson gson = new Gson();
            String json = settings.getString(Key, "");
            if (json.length() > 0) {
                json = Encryptor.decrypt(getEncryptionSP(), json);
                value = gson.fromJson(json, myClass);
            }
        } catch (Exception ex) {
        }
        return value;
    }

    static public boolean savePrefObject(Object obj, String Key, Context context) {
        SharedPreferences settings = context.getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor prefsEditor = settings.edit();
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        try {
            if (json.length() > 0)
                json = Encryptor.encrypt(getEncryptionSP(), json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        prefsEditor.putString(Key, json);
        prefsEditor.commit();
        return true;
    }

    static public boolean clearPref(Context c) {
        SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        return true;
    }

    static public boolean savePre(Context c, String value, String key) {
        try {
            if (value.length() > 0)
                value = Encryptor.encrypt(getEncryptionSP(), value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
        return true;
    }

    static public String getEncryptionSP() {
        String prefEncKey = "";
        try {
            prefEncKey = Config.EncryptionKey.substring(1, 7) +
                    Config.EncryptionKey.substring(3, 8) +
                    Config.EncryptionKey.substring(1, 3) +
                    Config.EncryptionKey.substring(7, 10);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return prefEncKey;
    }

    static public boolean saveLinkToPref(Context c, String value) {
        SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Config.PREF_KEY_APP_LINK, value);
        editor.commit();
        return true;
    }

    static public String getLinkFromPref(Context c) {
        String link = "";
        try {
            SharedPreferences settings = c.getSharedPreferences(Config.PREFS_NAME, 0);
            //http://nametag.developmenthouse.net/API/
            link = settings.getString(Config.PREF_KEY_APP_LINK, "http://192.168.1.104:8000");
//            link = settings.getString(Config.PREF_KEY_APP_LINK, "http://10.153.1.158:8000");
        } catch (Exception ex) {
        }
        return link;
    }

    public static String getIMEI(Context context) {
        String IMEI = Methods.getPref(context, Config.PREF_KEY_IMEI);
        if (IMEI.length() == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED) {

                    String serviceName = Context.TELEPHONY_SERVICE;
                    TelephonyManager m_telephonyManager = (TelephonyManager) context.getSystemService(serviceName);
                    IMEI = m_telephonyManager.getDeviceId();
                    if (IMEI == null) {
                        IMEI = "";
                    } else {
                        Methods.savePre(context, IMEI, Config.PREF_KEY_IMEI);
                    }
                }
            } else {
                String serviceName = Context.TELEPHONY_SERVICE;
                TelephonyManager m_telephonyManager = (TelephonyManager) context.getSystemService(serviceName);
                IMEI = m_telephonyManager.getDeviceId();
                if (IMEI == null) {
                    IMEI = "";
                } else {
                    Methods.savePre(context, IMEI, Config.PREF_KEY_IMEI);
                }
            }
        }
        return IMEI;
    }

    public static String getLanguageNumber() {

        String lang = "1";
        String localePhone = Locale.getDefault().getLanguage();
        if (localePhone.equalsIgnoreCase("EN"))
            lang = "1";
        else if (localePhone.equalsIgnoreCase("AR"))
            lang = "3";

        return lang;
    }

    static public String getOSversion() {
        return Build.VERSION.RELEASE;
    }

    public static String getAppVersion(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String Appversion = pInfo.versionName;
        return Appversion;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String findSimID(Context con) {
        String SimID = "";
        try {
            String serviceName = Context.TELEPHONY_SERVICE;
            TelephonyManager m_telephonyManager = (TelephonyManager) con
                    .getSystemService(serviceName);
            SimID = m_telephonyManager.getSubscriberId();//getSimSerialNumber();
            if (SimID == null) {
                SimID = "";
            }
        } catch (Exception ex) {
            SimID = "";
        }
        if (SimID.equals("")) {
            SimID = Methods.getPref(con, Config.PREF_KEY_IMSI_REG);
        }
        return "" + SimID;
    }

    static public String Get_MCC(Context con) {  /** MOBILE COUNTRY CODE **/
        int mcc = 0;
        try {
            TelephonyManager tel = (TelephonyManager) con.getSystemService(Context.TELEPHONY_SERVICE);
            String networkOperator = tel.getSimOperator();

            if (networkOperator != null) {
                mcc = Integer.parseInt(networkOperator.substring(0, 3));
            }
        } catch (Exception x) {
        }
        return "" + mcc;
    }

    static public String Get_MNC(Context con) {  /** MOBILE COUNTRY CODE **/
        int mnc = 0;
        try {
            TelephonyManager tel = (TelephonyManager) con.getSystemService(Context.TELEPHONY_SERVICE);
            String networkOperator = tel.getSimOperator();

            if (networkOperator != null) {
                mnc = Integer.parseInt(networkOperator.substring(3)); // MOBILE NETWORK CODE
            }
        } catch (Exception x) {
        }
        return "" + mnc;
    }

    public static String getInternationlFormatREG(Context mcontext, String mo, String countryISOGlobal) {
        String phoneInternational = mo;
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber pn;
            pn = phoneUtil.parse(mo, countryISOGlobal.toUpperCase());
            phoneInternational = phoneUtil.format(pn, PhoneNumberUtil.PhoneNumberFormat.E164);
            phoneInternational = PhoneNumberUtil.normalizeDigitsOnly(phoneInternational);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        return phoneInternational;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public static String doYourWork(Context mContext) {
        //StaticKey.Substring(6, 4) & imei.Substring(1, 6) & StaticKey.Substring(3, 6)
        // staticKey.Substring(6, 4) + imei.Substring(1, 6) +    staticKey.Substring(3, 6)


        String pushId = Methods.getPref(mContext, Config.PREF_KEY_FCM_PUSHID);
        String pushKey;
        String staticKey1 = Config.EncryptionKey.substring(6, 10);
        if (pushId == null || pushId.equals(""))
            pushKey = Methods.getIMEI(mContext).substring(1, 7);
        else
            pushKey = pushId.substring(1, 7);
        String staticKey2 = Config.EncryptionKey.substring(3, 9);
        return staticKey1 + pushKey + staticKey2;
    }

    public static String removeDoubleQuotes(String input_text) {
        String output_text = input_text;
        if (output_text.startsWith("\"")) {
            output_text = output_text.substring(1);
        }
        if (output_text.endsWith("\"")) {
            output_text = output_text.substring(0, output_text.length() - 1);
        }
        if (output_text.startsWith("\"") || output_text.endsWith("\"")) {
            removeDoubleQuotes(output_text);
        }
        return output_text;
    }

    public static void showCustomDialog(Context context, Dialog alertDialog, String msgStr, String negativeStr, String positiveStr, View.OnClickListener negativeListener, View.OnClickListener positiveListener, boolean showPositiveBtn, View.OnClickListener neutralListener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.alert_dialog_layout, null);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.windowAnimations = R.style.share_dialog_anim;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(wlp);
        alertDialog.setContentView(dialogView);

        LinearLayout mainLin = dialogView.findViewById(R.id.mainLin);
        AppCompatTextView dialogMsgTxt = dialogView.findViewById(R.id.dialogMsgTxt);
        AppCompatTextView negativeBtn = dialogView.findViewById(R.id.negativeBtn);
        AppCompatTextView positiveBtn = dialogView.findViewById(R.id.positiveBtn);
        AppCompatTextView neutralBtn = dialogView.findViewById(R.id.neutral_button);
        if (!showPositiveBtn) {
            positiveBtn.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            negativeBtn.setLayoutParams(lp);
        }
        dialogMsgTxt.setText(msgStr);
        negativeBtn.setText(negativeStr);
        positiveBtn.setText(positiveStr);
        negativeBtn.setOnClickListener(negativeListener);
        positiveBtn.setOnClickListener(positiveListener);
        neutralBtn.setOnClickListener(neutralListener);

        mainLin.getLayoutParams().height = (int) (Methods.getScreenHeight(context) / 3.5);
        alertDialog.show();

    }

    public static void customDialog(Context context, Dialog alertDialog, String msgStr, String negativeStr, String positiveStr, View.OnClickListener negativeListener, View.OnClickListener positiveListener, boolean showPositiveBtn, View.OnClickListener neutralListener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.alert_dialog_layout, null);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.windowAnimations = R.style.share_dialog_anim;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(wlp);
        alertDialog.setContentView(dialogView);

        LinearLayout mainLin = dialogView.findViewById(R.id.mainLin);
        ImageView imageView = dialogView.findViewById(R.id.imageIcon);
        AppCompatTextView dialogMsgTxt = dialogView.findViewById(R.id.dialogMsgTxt);
        AppCompatTextView negativeBtn = dialogView.findViewById(R.id.negativeBtn);
        AppCompatTextView positiveBtn = dialogView.findViewById(R.id.positiveBtn);
        AppCompatTextView neutralBtn = dialogView.findViewById(R.id.neutral_button);
        if (!showPositiveBtn) {
            positiveBtn.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            negativeBtn.setLayoutParams(lp);
        }
        imageView.setVisibility(View.INVISIBLE);
        dialogMsgTxt.setText(msgStr);
        negativeBtn.setText(negativeStr);
        positiveBtn.setText(positiveStr);
        negativeBtn.setOnClickListener(negativeListener);
        positiveBtn.setOnClickListener(positiveListener);
        neutralBtn.setOnClickListener(neutralListener);

        mainLin.getLayoutParams().height = (int) (Methods.getScreenHeight(context) / 4.5);
        alertDialog.show();

    }


}
