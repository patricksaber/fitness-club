package pinning.com.fitnessclub.utils;

import java.io.ByteArrayOutputStream;

/**
 * 
 * @author Claude
 */
public class b64 {

	private static char[] map1 = new char[64];
	static {
		int i = 0;
		for (char c = 'A'; c <= 'Z'; c++) {
			map1[i++] = c;
		}
		for (char c = 'a'; c <= 'z'; c++) {
			map1[i++] = c;
		}
		for (char c = '0'; c <= '9'; c++) {
			map1[i++] = c;
		}
		map1[i++] = '+';
		map1[i++] = '/';
	}

	private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		+ "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "+/";

	private static final int splitLinesAt = 76;

	public static byte[] zeroPad(int length, byte[] bytes) {
		byte[] padded = new byte[length]; // initialized to zero by JVM
		System.arraycopy(bytes, 0, padded, 0, bytes.length);
		return padded;
	}


	public static String encode(String string) {

		String encoded = "";
		byte[] stringArray;
		try {
			stringArray = string.getBytes("UTF-8");  // use appropriate encoding string!
		} catch (Exception ignored) {
			stringArray = string.getBytes();  // use locale default rather than croak
		}
		// determine how many padding bytes to add to the output
		int paddingCount = (3 - (stringArray.length % 3)) % 3;
		// add any necessary padding to the input
		stringArray = zeroPad(stringArray.length + paddingCount, stringArray);
		// process 3 bytes at a time, churning out 4 output bytes
		// worry about CRLF insertions later
		for (int i = 0; i < stringArray.length; i += 3) {
			int j = ((stringArray[i] & 0xff) << 16) +
			((stringArray[i + 1] & 0xff) << 8) + 
			(stringArray[i + 2] & 0xff);
			encoded = encoded + base64code.charAt((j >> 18) & 0x3f) +
			base64code.charAt((j >> 12) & 0x3f) +
			base64code.charAt((j >> 6) & 0x3f) +
			base64code.charAt(j & 0x3f);
		}
		// replace encoded padding nulls with "="
		return splitLines(encoded.substring(0, encoded.length() -
				paddingCount) + "==".substring(0, paddingCount));

	}
	public static String splitLines(String string) {

		String lines = "";
		for (int i = 0; i < string.length(); i += splitLinesAt) {

			lines += string.substring(i, Math.min(string.length(), i + splitLinesAt));
			lines += "\r\n";

		}
		return lines;

	}
	public static String encode1(byte[] in ) {
		int     iLen      = in.length;
		int     oDataLen  = ( iLen * 4 + 2 ) / 3;// output length without padding
		int     oLen      = ( ( iLen + 2 ) / 3 ) * 4;// output length including padding
		char[]  out       = new char[oLen];
		int     ip        = 0;
		int     op        = 0;
		int     i0;
		int     i1;
		int     i2;
		int     o0;
		int     o1;
		int     o2;
		int     o3;
		while ( ip < iLen ) {
			i0 = in[ip++] & 0xff;
			i1 = ip < iLen ? in[ip++] & 0xff : 0;
			i2 = ip < iLen ? in[ip++] & 0xff : 0;
			o0 = i0 >>> 2;
			o1 = ( ( i0 & 3 ) << 4 ) | ( i1 >>> 4 );
			o2 = ( ( i1 & 0xf ) << 2 ) | ( i2 >>> 6 );
			o3 = i2 & 0x3F;
			out[op++] = map1[o0];
			out[op++] = map1[o1];
			out[op] = op < oDataLen ? map1[o2] : '=';
			op++;
			out[op] = op < oDataLen ? map1[o3] : '=';
			op++;
		}
		return new String( out );
	}
	public static byte [] decode1 (String s) {

		int i = 0;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int len = s.length ();

		while (true) {
			while (i < len && s.charAt (i) <= ' ') i++;

			if (i == len) break;

			int tri = (decode (s.charAt (i)) << 18)
			+ (decode (s.charAt (i+1)) << 12)
			+ (decode (s.charAt (i+2)) << 6)
			+ (decode (s.charAt (i+3)));

			bos.write ((tri >> 16) & 255);
			if (s.charAt (i+2) == '=') break;
			bos.write ((tri >> 8) & 255);
			if (s.charAt (i+3) == '=') break;
			bos.write (tri & 255);

			i += 4;
		}
		return bos.toByteArray ();
	}


	public static String base64Encode(byte[] in) {
		int iLen = in.length;
		int oDataLen = (iLen * 4 + 2) / 3;// output length without padding
		int oLen = ((iLen + 2) / 3) * 4;// output length including padding
		char[] out = new char[oLen];
		int ip = 0;
		int op = 0;
		int i0;
		int i1;
		int i2;
		int o0;
		int o1;
		int o2;
		int o3;
		while (ip < iLen) {
			i0 = in[ip++] & 0xff;
			i1 = ip < iLen ? in[ip++] & 0xff : 0;
			i2 = ip < iLen ? in[ip++] & 0xff : 0;
			o0 = i0 >>> 2;
			o1 = ((i0 & 3) << 4) | (i1 >>> 4);
			o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
			o3 = i2 & 0x3F;
			out[op++] = map1[o0];
			out[op++] = map1[o1];
			out[op] = op < oDataLen ? map1[o2] : '=';
			op++;
			out[op] = op < oDataLen ? map1[o3] : '=';
			op++;
		}
		return new String(out);
	}

	public static byte[] Base64Decode(String s) {

		int i = 0;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int len = s.length();

		while (true) {
			while (i < len && s.charAt(i) <= ' ')
				i++;

			if (i == len)
				break;

			int tri = (decode(s.charAt(i)) << 18)
			+ (decode(s.charAt(i + 1)) << 12)
			+ (decode(s.charAt(i + 2)) << 6)
			+ (decode(s.charAt(i + 3)));

			bos.write((tri >> 16) & 255);
			if (s.charAt(i + 2) == '=')
				break;
			bos.write((tri >> 8) & 255);
			if (s.charAt(i + 3) == '=')
				break;
			bos.write(tri & 255);

			i += 4;
		}
		return bos.toByteArray();
	}

	private static int decode(char c) {
		if (c >= 'A' && c <= 'Z')
			return c - 65;
		else if (c >= 'a' && c <= 'z')
			return c - 97 + 26;
		else if (c >= '0' && c <= '9')
			return c - 48 + 26 + 26;
		else
			switch (c) {
			case '+':
				return 62;
			case '/':
				return 63;
			case '=':
				return 0;
			default:
				throw new RuntimeException(
						new StringBuffer("unexpected code: ").append(c)
						.toString());
			}
	}

}