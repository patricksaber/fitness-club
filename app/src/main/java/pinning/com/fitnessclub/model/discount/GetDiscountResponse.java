
package pinning.com.fitnessclub.model.discount;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

import pinning.com.fitnessclub.model.StatusCode;
@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class GetDiscountResponse {

    @SerializedName("data")
    @Expose
    private List<DiscountData> data = null;
    @SerializedName("statusCode")
    @Expose
    private StatusCode statusCode;

    public List<DiscountData> getDiscountData() {
        return data;
    }

    public void setDiscountData(List<DiscountData> discountData) {
        this.data = discountData;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

}
