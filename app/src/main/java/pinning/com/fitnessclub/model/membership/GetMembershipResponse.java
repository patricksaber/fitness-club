
package pinning.com.fitnessclub.model.membership;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

import pinning.com.fitnessclub.model.StatusCode;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class GetMembershipResponse {

    @SerializedName("data")
    @Expose
    private List<MembershipData> data = null;
    @SerializedName("statusCode")
    @Expose
    private StatusCode statusCode;

    public List<MembershipData> getData() {
        return data;
    }

    public void setData(List<MembershipData> data) {
        this.data = data;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

}
