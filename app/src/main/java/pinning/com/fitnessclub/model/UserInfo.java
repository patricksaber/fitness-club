package pinning.com.fitnessclub.model;

/**
 * Created by cobra on 8/6/18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class UserInfo {

    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("accountType")
    @Expose
    private String accountType;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("qrcode")
    @Expose
    private String qrcode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

}