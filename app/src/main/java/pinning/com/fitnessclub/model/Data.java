package pinning.com.fitnessclub.model;

/**
 * Created by cobra on 8/6/18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class Data {

    @SerializedName("gymId")
    @Expose
    private String gymId;
    @SerializedName("gymName")
    @Expose
    private String gymName;
    @SerializedName("gymNumber")
    @Expose
    private String gymNumber;
    @SerializedName("gymLocation")
    @Expose
    private String gymLocation;
    @SerializedName("gymTime")
    @Expose
    private String gymTime;
    @SerializedName("gymX")
    @Expose
    private String gymX;
    @SerializedName("gymY")
    @Expose
    private String gymY;
    @SerializedName("image")
    @Expose
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGymId() {
        return gymId;
    }

    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    public String getGymName() {
        return gymName;
    }

    public void setGymName(String gymName) {
        this.gymName = gymName;
    }

    public String getGymNumber() {
        return gymNumber;
    }

    public void setGymNumber(String gymNumber) {
        this.gymNumber = gymNumber;
    }

    public String getGymLocation() {
        return gymLocation;
    }

    public void setGymLocation(String gymLocation) {
        this.gymLocation = gymLocation;
    }

    public String getGymTime() {
        return gymTime;
    }

    public void setGymTime(String gymTime) {
        this.gymTime = gymTime;
    }

    public String getGymX() {
        return gymX;
    }

    public void setGymX(String gymX) {
        this.gymX = gymX;
    }

    public String getGymY() {
        return gymY;
    }

    public void setGymY(String gymY) {
        this.gymY = gymY;
    }

}
