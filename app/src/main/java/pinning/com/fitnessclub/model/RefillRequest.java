package pinning.com.fitnessclub.model;

/**
 * Created by cobra on 8/22/18.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class RefillRequest {

    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("cardId")
    @Expose
    private String cardId;
    @SerializedName("cardType")
    @Expose
    private String cardType;
    @SerializedName("pushId")
    @Expose
    private String pushId;

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

}
