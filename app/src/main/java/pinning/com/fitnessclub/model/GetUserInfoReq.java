package pinning.com.fitnessclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class GetUserInfoReq {

    @SerializedName("userName")
    @Expose
    private String userName;

    public String getUsername() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

}