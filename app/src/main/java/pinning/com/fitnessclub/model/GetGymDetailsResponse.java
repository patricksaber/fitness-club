package pinning.com.fitnessclub.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;
import org.parceler.Parcel;

import java.util.List;

@Generated("org.jsonschema2pojo")
@Parcel(Parcel.Serialization.BEAN)
public class GetGymDetailsResponse {
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("statusCode")
    @Expose
    private StatusCode statusCode;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

}