package pinning.com.fitnessclub.adapter;

/**
 * Created by cobra on 7/23/18.
 */


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by koa on 9/21/2015.
 */
public class FragmentPageAdapter extends FragmentStatePagerAdapter {

    List<MainFragmentModel> fragmentModels = new ArrayList<>();

    Activity mContext;

    public FragmentPageAdapter(Activity context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentModels.get(position).fragment;
    }

    @Override
    public int getCount() {
        return fragmentModels.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentModels.get(position).title;
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentModels.add(new MainFragmentModel(fragment, title));
        //notifyDataSetChanged();
    }

    public class MainFragmentModel {

        public final Fragment fragment;
        public final String title;


        public MainFragmentModel(Fragment fragment, String title) {
            this.fragment = fragment;
            this.title = title;
        }
    }

}