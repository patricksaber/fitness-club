package pinning.com.fitnessclub.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.contract.GymsDetailsContract;
import pinning.com.fitnessclub.model.GetGymDetailsResponse;
import pinning.com.fitnessclub.utils.CursorRecyclerViewAdapter;

/**
 * Created by cobra on 7/19/18.
 */

public class GymAdapter extends CursorRecyclerViewAdapter<GymAdapter.ViewHolder> {

    Context context;
    OnLocateGym onLocateGym;
    OnLocateClasses onLocateClasses;

    public GymAdapter(Context context,Cursor cursor,OnLocateGym onLocateGym,OnLocateClasses onLocateClasses){
        super(context,cursor);
        this.context = context;
        setOnLocateGym(onLocateGym);
        setOnLocateClasses(onLocateClasses);

    }

    public interface OnLocateGym{
        void onClickLocation(int position);
    }

    public interface OnLocateClasses{
        void onClickClasses(int position);
    }

    public  void setOnLocateGym(OnLocateGym llistener){
        onLocateGym = llistener;
    }
    public  void setOnLocateClasses(OnLocateClasses locateClasses){
        onLocateClasses = locateClasses;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gym_list_row,parent,false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        String gymNamec      = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_NAME));
        String gymLocationc  = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_LOCATION));
        String gymNumberc    = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_NUMBER));
        String gymOpeningc   = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_TIME));
        String gymXc         = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_X));
        String gymYc         = cursor.getString(cursor.getColumnIndex(GymsDetailsContract.Entry.COL_GYM_Y));
        viewHolder.gymName.setText(gymNamec);
        viewHolder.gymLocation.setText(gymLocationc);
        viewHolder.gymNumber.setText(gymNumberc);
        viewHolder.gymOpening.setText(gymOpeningc);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView gymName,gymLocation,gymNumber,gymOpening;
        private AppCompatButton direction ,classes;
        ViewHolder(View itemView) {
            super(itemView);
            gymName = itemView.findViewById(R.id.gym_name);
            gymLocation= itemView.findViewById(R.id.gym_location);
            gymNumber = itemView.findViewById(R.id.gym_number);
            gymOpening = itemView.findViewById(R.id.gym_opening);
            direction = itemView.findViewById(R.id.direction);
            classes = itemView.findViewById(R.id.gymSchedule);
            classes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLocateClasses != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onLocateClasses.onClickClasses(position);


                        }
                    }
                }
            });
            direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLocateGym != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            onLocateGym.onClickLocation(position);


                        }
                    }
                }
            });
        }
    }
}
