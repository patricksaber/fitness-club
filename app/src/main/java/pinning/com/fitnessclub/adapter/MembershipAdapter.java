package pinning.com.fitnessclub.adapter;


import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import pinning.com.fitnessclub.R;
import pinning.com.fitnessclub.contract.MembershipContract;
import pinning.com.fitnessclub.utils.CursorRecyclerViewAdapter;

/**
 * Created by cobra on 7/19/18.
 */

public class MembershipAdapter extends CursorRecyclerViewAdapter<MembershipAdapter.ViewHolder> {

    Context context;

    OnLocateDiscount locateDiscount;


    public MembershipAdapter(Context context, Cursor cursor, OnLocateDiscount onLocateGym) {
        super(context, cursor);
        this.context = context;
        setOnLocateGym(onLocateGym);


    }

    public interface OnLocateDiscount {
        void onClickDiscount(int position);
    }


    public void setOnLocateGym(OnLocateDiscount llistener) {
        locateDiscount = llistener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mem_row, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        viewHolder.membership_exp.setVisibility(View.INVISIBLE);
        String title = cursor.getString(cursor.getColumnIndex(MembershipContract.Entry.COL_MEMBERSHIP_TITILE));
        String price = cursor.getString(cursor.getColumnIndex(MembershipContract.Entry.COL_MEMBERSHIP_PRICE));
        String priority = cursor.getString(cursor.getColumnIndex(MembershipContract.Entry.COL_MEMBERSHIP_PRIORITY));
        viewHolder.title.setText(title);
        viewHolder.price.setText(price);

        if (priority.contentEquals("diamond")) {
            viewHolder.membership_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.card_view_diamond_selector));
        } else if (priority.contentEquals("gold")) {
            viewHolder.membership_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.card_view_gold_selector));
        } else
            viewHolder.membership_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.card_view_basic_selector));

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout membership_layout;
        private AppCompatTextView title, membership_exp, price;

        ViewHolder(View itemView) {
            super(itemView);
            membership_layout = itemView.findViewById(R.id.membership_layout);
            title = itemView.findViewById(R.id.title);
            membership_exp = itemView.findViewById(R.id.membership_exp);
            price = itemView.findViewById(R.id.price);

            membership_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (locateDiscount != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            locateDiscount.onClickDiscount(position);
                        }
                    }
                }
            });
        }
    }
}
